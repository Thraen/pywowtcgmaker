#!/usr/bin/python3
# -*- coding: utf-8 -*-

import discord
import os
import re
import sqlite3
from itertools import combinations
from collections import Counter

def lookup(name_keywords, set_keywords):
    con = sqlite3.connect("/home/thra/pywowtcgmaker/sqlite.db")
    c = con.cursor()

    for word_index in range(len(name_keywords)):
        name_keywords[word_index] = name_keywords[word_index].replace("’", "'")

    for word in range(len(name_keywords), 0, -1):
        for comb in combinations(name_keywords, word):
            print(comb)
            wild_name_keywords = []
            for keyword in comb:
                wild_name_keywords.append("%" + keyword + "%")
            wild_set_keywords = []
            for keyword in set_keywords:
                wild_set_keywords.append("%" + keyword + "%")

            query_text = "SELECT name, setname, type FROM cards WHERE setname != 'Archive' AND setname NOT LIKE '%Class Starter%' AND "
            query_text += " AND ".join(["name LIKE ?" for _ in wild_name_keywords])
            if set_keywords:
                query_text += " AND "
                query_text += " AND ".join(["setname LIKE ?" for _ in wild_set_keywords])

            query = c.execute(query_text, wild_name_keywords + wild_set_keywords)
            results = query.fetchall()

            if results:
                break
        if results:
            break

    if not results:
        return "No path", ""
    con.close()

    closest_result = results[0]
    closest_match_size = 0
    print(results)

    for result in results:
        counters = [Counter(s) for s in [" ".join(name_keywords), result[0].lower()]]
        match_size = sum((counters[0] & counters[1]).values())
        if match_size > closest_match_size or (match_size == closest_match_size and len(result[0]) < len(closest_result[0])):
            closest_result = result
            closest_match_size = match_size

    art_path = closest_result[1] + "/" + closest_result[0]

    return art_path, closest_result[2]

class WoWTCGMakerBot(discord.Client):
    def __init__(self):
        bot_intents = discord.Intents.default()
        bot_intents.messages = True
        super().__init__(intents=bot_intents)

    async def on_ready(self):
        print('Logged on as', self.user)

    async def on_message(self, message):
        # Don't respond to ourselves #
        if message.author == self.user:
            return

        msg = message.content
        print(msg)

        if msg and msg[0] != "!" and "[[" in msg and "]]" in msg:
            keyword_blocks = re.findall(r"\[\[.*?\]\]", msg)
            for block in keyword_blocks:
                set_keywords = re.findall(r"\(.*?\)", block)
                if set_keywords:
                    set_keywords = set_keywords[0]
                    block = block.replace(set_keywords, "")
                    set_keywords = list(filter("".__ne__, re.split(" ", set_keywords[1:-1])))
                name_keywords = list(filter("".__ne__, re.split(" ", block[2:-2])))
                if name_keywords:
                    for i in range(len(name_keywords)):
                        name_keywords[i] = name_keywords[i].lower()
                    art_path, card_type = lookup(name_keywords, set_keywords)
                else:
                    continue
                if art_path == "No path":
                    await message.channel.send(block + " card has not been found in the database")
                else:
                    card_art_path_png = "Scans/" + art_path + ".png"
                    card_art_path_jpg = "Scans/" + art_path + ".jpg"

                    if card_type == "Hero":
                        card_back_art_path_png = "Scans/" + art_path + " - Backside.png"
                        card_back_art_path_jpg = "Scans/" + art_path + " - Backside.jpg"
                        if os.path.isfile(card_back_art_path_png):
                            await message.channel.send(file=discord.File(card_back_art_path_png))
                        elif os.path.isfile(card_back_art_path_jpg):
                            await message.channel.send(file=discord.File(card_back_art_path_jpg))
                    
                    if os.path.isfile(card_art_path_png):
                        await message.channel.send(file=discord.File(card_art_path_png))
                    elif os.path.isfile(card_art_path_jpg):
                        await message.channel.send(file=discord.File(card_art_path_jpg))

                    else:
                        print(art_path)

                        dev_file = open("/home/thra/pywowtcgmaker/dev_id", "r")
                        dev_id = dev_file.readline()
                        dev_file.close()

                        dev = await self.fetch_user(int(dev_id))
                        print(dev)
                        if dev:
                            await dev.send(art_path)

                        await message.channel.send(str(art_path) + " card has been found but it's scan is not available.\nIncident reported, Thraen will patch it asap")

bot = WoWTCGMakerBot()
bot_token_file = open("bot_token", "r")
bot_token = bot_token_file.readline()
bot_token_file.close()
bot.run(bot_token)
