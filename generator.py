#!/usr/bin/python3
# -*- coding: utf-8 -*-

from PIL import Image, ImageDraw, ImageFont, ImageOps
from math import ceil, floor
import json
import re

from os.path import exists, isfile

type_names = ["Ability", "Ally", "Equipment", "Hero", "Master Hero", "Location", "Quest", "Event"]
variant_names = ["None", "Extended Art", "Front", "Back", "High Side Bar", "Low Side Bar", "No Side Bar"]
faction_names = ["Alliance", "Horde", "Neutral", "Monster", "Scourge"]
classes_names = ["Death Knight", "Demon Hunter", "Druid", "Hunter", "Mage", "Monk", "Paladin", "Priest", "Rogue", "Shaman", "Warlock", "Warrior"] 
rarity_names = ["Common", "Uncommon", "Rare", "Epic"]

class Card():
    def __init__(self):
        # Define card values #
        self.name = ""
        self.card_type = "Ability"
        self.type_override = ""
        self.variant = "None"
        self.faction = "Neutral"
        self.class_icons = []
        self.cost = ""
        self.damage = ""
        self.damage_type = ""
        self.health = ""
        self.armor = ""
        self.strike_cost = ""
        self.capacity = ""
        self.instant = False
        self.subtypes = ""
        self.tags = ""
        self.professions = []
        self.effect = ""
        self.art_box = (0, 51, 406, 383)
        self.art = ""
        self.flavour = ""
        self.artist = ""
        self.copyright = ""
        self.rarity = "Common"
        self.block_number = ""
        self.set_name = ""
        self.set_number = "1"
        self.set_max_number = "1"
        self.border_color = (0, 0, 0, 255)
        self.border_sharp = False
        self.locale_name = "English"
        self.art_path = "Template/Dummy.png"
        self.background_path = ""
        self.locale = {}
        self.image = None

    def get_from_json(self, json_object, field):
        if field in json_object:
            return json_object[field]
        else:
            return ""

    def fromJson(self, json_object):  # Json object is indeed a dict
        self.name = self.get_from_json(json_object, "name")
        self.card_type = self.get_from_json(json_object, "card_type")
        # Legacy compat check #
        if self.card_type not in type_names:
            self.card_type = "Equipment"
        self.type_override = self.get_from_json(json_object, "type_override")
        self.variant = self.get_from_json(json_object, "variant")
        if not self.variant in variant_names and not isfile("Template/Custom/" + self.card_type + "/" + self.variant):
            self.variant = "None"
        self.faction = self.get_from_json(json_object, "faction")
        if not self.faction in faction_names:
            self.faction = "Neutral"
        filtered_classes = []
        for class_name in self.get_from_json(json_object, "class_icons"):
            if class_name in classes_names:
                filtered_classes.append(class_name)
        self.class_icons = filtered_classes
        if not self.class_icons:
            self.class_icons = []
        self.cost = self.get_from_json(json_object, "cost")
        self.damage = self.get_from_json(json_object, "damage")
        self.damage_type = self.get_from_json(json_object, "damage_type")
        self.health = self.get_from_json(json_object, "health")
        self.armor = self.get_from_json(json_object, "armor")
        self.strike_cost = self.get_from_json(json_object, "strike_cost")
        self.capacity = self.get_from_json(json_object, "capacity")
        self.instant = self.get_from_json(json_object, "instant")
        if not self.instant:
            self.instant = False
        self.subtypes = self.get_from_json(json_object, "subtypes")
        self.tags = self.get_from_json(json_object, "tags")
        self.professions = self.get_from_json(json_object, "professions")
        if not self.professions:
            self.professions = []
        self.effect = self.get_from_json(json_object, "effect")
        self.art_box = self.get_from_json(json_object, "art_box")
        if not self.art_box:
            self.art_box = ()
        self.flavour = self.get_from_json(json_object, "flavour")
        self.artist = self.get_from_json(json_object, "artist")
        self.copyright = self.get_from_json(json_object, "copyright")
        self.rarity = self.get_from_json(json_object, "rarity")
        if not self.rarity in rarity_names:
            self.rarity = "Common"
        self.block_number = self.get_from_json(json_object, "block_number")
        self.set_name = self.get_from_json(json_object, "set_name")
        self.set_number = self.get_from_json(json_object, "set_number")
        self.set_max_number = self.get_from_json(json_object, "set_max_number")
        self.border_color = tuple(self.get_from_json(json_object, "border_color"))
        if not self.border_color or len(self.border_color) != 4:
            self.border_color = (0, 0, 0, 255)
        self.border_sharp = self.get_from_json(json_object, "border_type")
        if not self.border_sharp:
            self.border_sharp = False
        self.locale_name = self.get_from_json(json_object, "locale_name")
        self.art_path = self.get_from_json(json_object, "art_path")
        if not self.art_path:
            self.art_path = "Template/Dummy.png"
        
    def toJson(self):  # Json object is indeed a dict
        json_object = {}
        json_object["name"] = self.name
        json_object["card_type"] = self.card_type
        json_object["type_override"] = self.type_override
        json_object["variant"] = self.variant
        json_object["faction"] = self.faction
        json_object["class_icons"] = self.class_icons
        json_object["cost"] = self.cost
        json_object["damage"] = self.damage
        json_object["damage_type"] = self.damage_type
        json_object["health"] = self.health
        json_object["armor"] = self.armor
        json_object["strike_cost"] = self.strike_cost
        json_object["capacity"] = self.capacity
        json_object["instant"] = self.instant
        json_object["subtypes"] = self.subtypes
        json_object["tags"] = self.tags
        json_object["professions"] = self.professions
        json_object["effect"] = self.effect
        json_object["art_box"] = self.art_box
        json_object["flavour"] = self.flavour
        json_object["artist"] = self.artist
        json_object["copyright"] = self.copyright
        json_object["rarity"] = self.rarity
        json_object["block_number"] = self.block_number
        json_object["set_name"] = self.set_name
        json_object["set_number"] = self.set_number
        json_object["set_max_number"] = self.set_max_number
        json_object["border_color"] = self.border_color
        json_object["border_sharp"] = self.border_sharp
        json_object["locale_name"] = self.locale_name
        json_object["art_path"] = self.art_path

        return json_object

    def status(self):
        status = ""
        status += "Name: " + self.name + "\n"
        status += "Type: " + self.card_type + "\n"
        status += "Variant: " + str(self.variant) + "\n"
        status += "Faction: " + self.faction + "\n"
        val = ""
        for class_icon in self.class_icons:
            val += class_icon + " "
        status += "Class Icons: " + val + "\n"
        status += "Cost: " + self.cost + "\n"
        status += "Damage: " + self.damage + "\n"
        status += "Damage Type: " + str(self.damage_type) + "\n"
        status += "Health: " + str(self.health) + "\n"
        status += "Armor: " + str(self.armor) + "\n"
        status += "Strike Cost: " + str(self.strike_cost) + "\n"
        status += "Capacity: " + str(self.capacity) + "\n"
        status += "Instant: " + str(self.instant) + "\n"
        status += "Subtypes: " + self.subtypes + "\n"
        status += "Tags: " + self.tags + "\n"
        val = ""
        for profession in self.professions:
            val += profession + " "
        status += "Professions: " + val + "\n"
        status += "Effect: " + self.effect + "\n"
        status += "Flavour: " + str(self.flavour) + "\n"
        status += "Artist: " + self.artist + "\n"
        status += "Copyright: " + self.copyright + "\n"
        status += "Rarity: " + str(self.rarity) + "\n"
        status += "Block number: " + str(self.block_number) + "\n"
        status += "Set Name: " + str(self.set_name) + "\n"
        status += "Set Number: " + str(self.set_number) + "\n"
        status += "Set Max Number: " + str(self.set_max_number) + "\n"
        status += "Border Color: " + self.border_color + "\n"
        status += "Border Type: " + self.border_type + "\n"

        return status

class WoWCardGenerator():
    def __init__(self):

        # Load default type names #
        locale_file = open("Locales/English.json")
        locale = json.load(locale_file)
        self.type_names = locale["type_names"]
        locale_file.close()

        # Load default font paths #
        self.main_font_path = "Template/Fonts/Latin/Belwe Bold BT.ttf"
        self.stat_font_path = "Template/Fonts/Latin/Belwe Bold BT.ttf"
        self.effect_font_path = "Template/Fonts/Latin/Trade Gothic LT Condensed No. 18.ttf"
        self.effect_bold_font_path = "Template/Fonts/Latin/Trade Gothic LT Bold Condensed No. 20.ttf"
        self.effect_italic_font_path = "Template/Fonts/Latin/Trade Gothic LT Condensed No. 18 Oblique.ttf"
        self.cost_font_path = "Template/Fonts/Latin/BelweStd-Condensed.otf"
        self.flavour_font_path = "Template/Fonts/Latin/Trade Gothic LT Light Oblique.ttf"
        self.artist_copyright_font_path = "Template/Fonts/Latin/Belmar-CondensedLight-Normal.ttf"
        self.set_font_path = "Template/Fonts/Latin/Uniform Condensed Bold.ttf"

        # Import template images #
        # ToDo? : pre-resize images #
        # Card borders #
        self.border_foreground = Image.open("Template/Border/Border Foreground.png")
        #self.border_sharp_foreground = Image.open("Template/Border/Border Sharp Foreground.png")
        self.border_background = Image.open("Template/Border/Border Background.png")
        self.border = Image.open("Template/Border/Border.png")
        self.border_sharp = Image.open("Template/Border/Border Sharp.png")

        # Ability frame and icons #
        self.ability_frame = Image.open("Template/Ability/Ability Frame.png").resize((690, 990), 1)
        self.ability_basic = Image.open("Template/Ability/Basic.png").resize((115, 39), 1)

        # Ally frames #
        self.ally_alliance_frame = Image.open("Template/Ally/Alliance Ally Frame.png").resize((690, 990), 1)
        self.ally_horde_frame = Image.open("Template/Ally/Horde Ally Frame.png").resize((690, 990), 1)
        self.ally_monster_frame = Image.open("Template/Ally/Monster Ally Frame.png").resize((690, 990), 1)
        self.ally_neutral_frame = Image.open("Template/Ally/Neutral Ally Frame.png").resize((690, 990), 1)
        self.ally_scourge_frame = Image.open("Template/Ally/Scourge Ally Frame.png").resize((690, 990), 1)

        # Equipment frames and icons #
        self.equipment_highsidebar_frame = Image.open("Template/Equipment/Equipment Frame High.png").resize((690, 990), 1)
        self.equipment_lowsidebar_frame = Image.open("Template/Equipment/Equipment Frame Low.png").resize((690, 990), 1)
        self.equipment_nosidebar_frame = Image.open("Template/Equipment/Equipment Frame No.png")
        self.equipment_background_classes = Image.open("Template/Equipment/Background Class Icons.png").resize((55, 622))
        self.equipment_class_death_knight = Image.open("Template/Equipment/Equipment Class Icons/Death Knight.png").resize((76, 53), 1)
        self.equipment_class_demon_hunter = Image.open("Template/Equipment/Equipment Class Icons/Demon Hunter.png").resize((76, 53), 1)
        self.equipment_class_druid = Image.open("Template/Equipment/Equipment Class Icons/Druid.png").resize((76, 53), 1)
        self.equipment_class_hunter = Image.open("Template/Equipment/Equipment Class Icons/Hunter.png").resize((76, 53), 1)
        self.equipment_class_mage = Image.open("Template/Equipment/Equipment Class Icons/Mage.png").resize((76, 53), 1)
        self.equipment_class_monk_alt = Image.open("Template/Equipment/Equipment Class Icons/Monk Alt.png").resize((76, 54), 1)
        self.equipment_class_paladin = Image.open("Template/Equipment/Equipment Class Icons/Paladin.png").resize((76, 53), 1)
        self.equipment_class_priest = Image.open("Template/Equipment/Equipment Class Icons/Priest.png").resize((76, 53), 1)
        self.equipment_class_rogue = Image.open("Template/Equipment/Equipment Class Icons/Rogue.png").resize((76, 53), 1)
        self.equipment_class_shaman = Image.open("Template/Equipment/Equipment Class Icons/Shaman.png").resize((76, 53), 1)
        self.equipment_class_warlock = Image.open("Template/Equipment/Equipment Class Icons/Warlock.png").resize((76, 53), 1)
        self.equipment_class_warrior = Image.open("Template/Equipment/Equipment Class Icons/Warrior.png").resize((76, 53), 1)
        self.equipment_health_icon = Image.open("Template/Equipment/Health.png").resize((136, 131), 1)
        self.equipment_armor_icon = Image.open("Template/Bottom Icons/Armor.png").resize((120, 128), 1)
        self.equipment_strike_icon = Image.open("Template/Bottom Icons/Strike.png").resize((129, 129), 1)

        # Hero frames and icons #
        # Back #
        self.hero_back_alliance = Image.open("Template/Hero/Back/Alliance Hero Back Frame.png").resize((690, 149), 1)
        self.hero_back_horde = Image.open("Template/Hero/Back/Horde Hero Back Frame.png").resize((690, 149), 1)
        self.hero_back_monster = Image.open("Template/Hero/Back/Monster Hero Back Frame.png").resize((690, 149), 1)
        self.hero_back_neutral = Image.open("Template/Hero/Back/Neutral Hero Back Frame.png").resize((690, 149), 1)
        self.hero_back_scourge = Image.open("Template/Hero/Back/Scourge Hero Back Frame.png").resize((690, 149), 1)
        self.hero_back_both_flag = Image.open("Template/Hero/Back/Both Flag.png").resize((56, 148), 1)

        self.hero_back_fix = Image.open("Template/Hero/Back/Frame Fix.png").resize((690, 149), 1)
        self.hero_back_text_background = Image.open("Template/Hero/Back/Text Background.png").resize((690, 1000), 1)

        self.hero_back_death_knight_icon = Image.open("Template/Class Icons/Death Knight.png").resize((86, 86), 1)
        self.hero_back_demon_hunter_icon = Image.open("Template/Class Icons/Demon Hunter.png").resize((86, 86), 1)
        self.hero_back_druid_icon = Image.open("Template/Class Icons/Druid.png").resize((86, 86), 1)
        self.hero_back_hunter_icon = Image.open("Template/Class Icons/Hunter.png").resize((86, 86), 1)
        self.hero_back_mage_icon = Image.open("Template/Class Icons/Mage.png").resize((86, 86), 1)
        self.hero_back_monk_icon = Image.open("Template/Class Icons/Monk.png").resize((86, 86), 1)
        self.hero_back_monk_alt_icon = Image.open("Template/Class Icons/Monk Alt.png").resize((91, 96), 1)
        self.hero_back_paladin_icon = Image.open("Template/Class Icons/Paladin.png").resize((86, 86), 1)
        self.hero_back_priest_icon = Image.open("Template/Class Icons/Priest.png").resize((86, 86), 1)
        self.hero_back_rogue_icon = Image.open("Template/Class Icons/Rogue.png").resize((86, 86), 1)
        self.hero_back_shaman_icon = Image.open("Template/Class Icons/Shaman.png").resize((86, 86), 1)
        self.hero_back_warlock_icon = Image.open("Template/Class Icons/Warlock.png").resize((86, 86), 1)
        self.hero_back_warrior_icon = Image.open("Template/Class Icons/Warrior.png").resize((86, 86), 1)
        # Front #
        self.hero_alliance = Image.open("Template/Hero/Front/Alliance Hero Frame.png").resize((690, 990), 1)
        self.hero_horde = Image.open("Template/Hero/Front/Horde Hero Frame.png").resize((690, 990), 1)
        self.hero_monster = Image.open("Template/Hero/Front/Monster Hero Frame.png").resize((690, 990), 1)
        self.hero_neutral = Image.open("Template/Hero/Front/Neutral Hero Frame.png").resize((690, 990), 1)
        self.hero_scourge = Image.open("Template/Hero/Front/Scourge Hero Frame.png").resize((690, 990), 1)
        self.hero_both_flag = Image.open("Template/Hero/Front/Both Flag.png").resize((120, 179), 1)

        self.hero_death_knight_icon = Image.open("Template/Class Icons/Death Knight.png").resize((118, 118), 1)
        self.hero_demon_hunter_icon = Image.open("Template/Class Icons/Demon Hunter.png").resize((122, 122), 1)
        self.hero_druid_icon = Image.open("Template/Class Icons/Druid.png").resize((118, 118), 1)
        self.hero_hunter_icon = Image.open("Template/Class Icons/Hunter.png").resize((118, 118), 1)
        self.hero_mage_icon = Image.open("Template/Class Icons/Mage.png").resize((118, 118), 1)
        self.hero_monk_icon = Image.open("Template/Class Icons/Monk.png").resize((118, 118), 1)
        self.hero_monk_alt_icon = Image.open("Template/Class Icons/Monk Alt.png").resize((129, 136), 1)
        self.hero_paladin_icon = Image.open("Template/Class Icons/Paladin.png").resize((118, 118), 1)
        self.hero_priest_icon = Image.open("Template/Class Icons/Priest.png").resize((118, 118), 1)
        self.hero_rogue_icon = Image.open("Template/Class Icons/Rogue.png").resize((118, 118), 1)
        self.hero_shaman_icon = Image.open("Template/Class Icons/Shaman.png").resize((118, 118), 1)
        self.hero_warlock_icon = Image.open("Template/Class Icons/Warlock.png").resize((118, 118), 1)
        self.hero_warrior_icon = Image.open("Template/Class Icons/Warrior.png").resize((118, 118), 1)
        # Class icons #
        self.death_knight_icon = Image.open("Template/Class Icons/Death Knight.png").resize((73, 73), 1)
        self.demon_hunter_icon = Image.open("Template/Class Icons/Demon Hunter.png").resize((73, 73), 1)
        self.druid_icon = Image.open("Template/Class Icons/Druid.png").resize((73, 73), 1)
        self.hunter_icon = Image.open("Template/Class Icons/Hunter.png").resize((73, 73), 1)
        self.mage_icon = Image.open("Template/Class Icons/Mage.png").resize((73, 73), 1)
        self.monk_icon = Image.open("Template/Class Icons/Monk.png").resize((73, 73), 1)
        self.monk_alt_icon = Image.open("Template/Class Icons/Monk Alt.png").resize((78, 82), 1)
        self.paladin_icon = Image.open("Template/Class Icons/Paladin.png").resize((73, 73), 1)
        self.priest_icon = Image.open("Template/Class Icons/Priest.png").resize((73, 73), 1)
        self.rogue_icon = Image.open("Template/Class Icons/Rogue.png").resize((73, 73), 1)
        self.shaman_icon = Image.open("Template/Class Icons/Shaman.png").resize((73, 73), 1)
        self.warlock_icon = Image.open("Template/Class Icons/Warlock.png").resize((73, 73), 1)
        self.warrior_icon = Image.open("Template/Class Icons/Warrior.png").resize((73, 73), 1)

        # Master hero frame and cost shield #
        self.master_hero_frame = Image.open("Template/Master Hero/Master Hero Frame.png").resize((690, 990), 1)
        self.master_hero_cost = Image.open("Template/Master Hero/Cost Shield.png").resize((160, 175), 1)

        # Location frame and icon #
        self.location_frame = Image.open("Template/Location/Location Frame.png").resize((690, 990), 1)
        self.location_capacity = Image.open("Template/Location/Location Capacity.png").resize((110, 115), 1)

        # Quest frame #
        self.quest_frame = Image.open("Template/Quest/Quest Frame.png").resize((690, 990), 1)
        self.quest_frame_no_icon = Image.open("Template/Quest/Quest Frame No Icon.png").resize((690, 990), 1)

        # Bottom icons #
        self.arcane_icon = Image.open("Template/Bottom Icons/Arcane.png").resize((139, 146), 1)
        self.chaos_icon = Image.open("Template/Bottom Icons/Chaos.png").resize((210, 183), 1).crop((17, 0, 210, 183)) #crop is a workaround, you can't alpha_composite() to negative values like paste()
        self.fire_icon = Image.open("Template/Bottom Icons/Fire.png").resize((138, 142), 1)
        self.frost_icon = Image.open("Template/Bottom Icons/Frost.png").resize((115, 125), 1)
        self.holy_icon = Image.open("Template/Bottom Icons/Holy.png").resize((116, 139), 1)
        self.melee_icon = Image.open("Template/Bottom Icons/Melee.png").resize((125, 135), 1)
        self.nature_icon = Image.open("Template/Bottom Icons/Nature.png").resize((131, 147), 1)
        self.ranged_icon = Image.open("Template/Bottom Icons/Ranged.png").resize((125, 131), 1)
        self.shadow_icon = Image.open("Template/Bottom Icons/Shadow.png").resize((115, 139), 1)

        # Top zone #
        self.alliance_banner = Image.open("Template/Top Zone/Alliance Banner.png").resize((65, 80), 1)
        self.horde_banner = Image.open("Template/Top Zone/Horde Banner.png").resize((65, 80), 1)
        self.monster_banner = Image.open("Template/Top Zone/Monster Banner.png").resize((65, 80), 1)
        self.scourge_banner = Image.open("Template/Top Zone/Scourge Banner.png").resize((65, 80), 1)

        # Bottom zone #
        self.bottom_bar = Image.open("Template/Bottom Zone/Bottom Bar.png").resize((690, 67), 1)
        self.stash_plate = Image.open("Template/Bottom Zone/Stash Plate.png")

        # Text box icons #
        self.text_exhaust = Image.open("Template/Text Box Icons/Exhaust.png").resize((50, 35), 1)
        self.text_numeric_cost = Image.open("Template/Text Box Icons/Numeric Cost.png")
        self.text_numeric_cost_invert = Image.open("Template/Text Box Icons/Numeric Cost Invert.png")
        self.text_power_invert = Image.open("Template/Text Box Icons/Power Invert.png")

        # Extended art #
        self.ea_ability_frame = Image.open("Template/Extended Art/Ability EA Frame.png").resize((690, 990), 1)
        self.ea_alliance_ally_frame = Image.open("Template/Extended Art/Alliance Ally EA Frame.png").resize((690, 990), 1)
        self.ea_horde_ally_frame = Image.open("Template/Extended Art/Horde Ally EA Frame.png").resize((690, 990), 1)
        self.ea_monster_ally_frame = Image.open("Template/Extended Art/Monster Ally EA Frame.png").resize((690, 990), 1)
        self.ea_neutral_ally_frame = Image.open("Template/Extended Art/Neutral Ally EA Frame.png").resize((690, 990), 1)
        self.ea_scourge_ally_frame = Image.open("Template/Extended Art/Scourge Ally EA Frame.png").resize((690, 990), 1)
        self.ea_equipment_frame = Image.open("Template/Extended Art/Equipment EA Frame.png").resize((690, 990), 1)

        self.ea_arcane_icon = Image.open("Template/Bottom Icons/Arcane.png").resize((106, 111), 1)
        self.ea_chaos_icon = Image.open("Template/Bottom Icons/Chaos.png").resize((160, 140), 1).crop((17, 0, 210, 183)) #crop is a workaround, you can't alpha_composite() to negative values like paste()
        self.ea_fire_icon = Image.open("Template/Bottom Icons/Fire.png").resize((105, 108), 1)
        self.ea_frost_icon = Image.open("Template/Bottom Icons/Frost.png").resize((88, 95), 1)
        self.ea_holy_icon = Image.open("Template/Bottom Icons/Holy.png").resize((88, 106), 1)
        self.ea_melee_icon = Image.open("Template/Bottom Icons/Melee.png").resize((95, 103), 1)
        self.ea_nature_icon = Image.open("Template/Bottom Icons/Nature.png").resize((100, 112), 1)
        self.ea_ranged_icon = Image.open("Template/Bottom Icons/Ranged.png").resize((95, 100), 1)
        self.ea_shadow_icon = Image.open("Template/Bottom Icons/Shadow.png").resize((88, 106), 1)

        self.ea_armor_icon = Image.open("Template/Bottom Icons/Armor.png").resize((91, 98), 1)
        self.ea_strike_icon = Image.open("Template/Bottom Icons/Strike.png").resize((98, 98), 1)

        # Set icons #
        # ToDo: add icons imports (extract from cards)
        self.set_icon_common = Image.open("Template/Set Icons/Set Icon Common.png").resize((41, 30), 1)
        self.set_icon_uncommon = Image.open("Template/Set Icons/Set Icon Uncommon.png").resize((41, 30), 1)
        self.set_icon_rare = Image.open("Template/Set Icons/Set Icon Rare.png").resize((41, 30), 1)
        self.set_icon_epic = Image.open("Template/Set Icons/Set Icon Epic.png").resize((41, 30), 1)

        # Rarity colors #
        self.common_color = (255, 255, 255, 255)
        self.uncommon_color = (0, 128, 64, 255)
        self.rare_color = (0, 112, 221, 255)
        self.epic_color = (163, 53, 238, 255)

        # Instant border and plate #
        self.instant_border = Image.open("Template/Instant/Instant Border.png").resize((710, 999), 1)
        self.instant_plate = Image.open("Template/Instant/Instant Plate.png").resize((114, 41), 1)

    def createCard(self, card, bleeding=[750, 1050]):
        # Load locale (defaults to English) #
        if not card.locale_name or not exists("Locales/" + card.locale_name + ".json"):
            card.locale_name = "English"

        locale_file = open("Locales/" + card.locale_name + ".json", encoding="utf-8")
        card.locale = json.load(locale_file)
        locale_file.close()

        # Switch fonts if necessary #
        if card.locale_name == "Chinese":
            self.main_font_path = "Template/Fonts/Chinese/Simli.ttf"
            self.effect_font_path = "Template/Fonts/Chinese/Simhei.ttf"
            self.effect_bold_font_path = "Template/Fonts/Chinese/Simhei-Bold.ttf"
            self.effect_italic_font_path = "Template/Fonts/Chinese/Simkai.ttf"
            self.flavour_font_path = "Template/Fonts/Chinese/Simkai.ttf"

        if card.locale_name == "Russian":
            self.main_font_path = "Template/Fonts/Cyrillic/belwebdbtrusbyme_bold.otf"
            self.effect_font_path = "Template/Fonts/Cyrillic/Sign.ttf"
            self.effect_bold_font_path = "Template/Fonts/Cyrillic/Manrope Bold.ttf"
            self.effect_italic_font_path = "Template/Fonts/Cyrillic/Mysl Italic.ttf"
            self.flavour_font_path = "Template/Fonts/Cyrillic/Piazzolla Italic.ttf"

        else:
            self.main_font_path = "Template/Fonts/Latin/Belwe Bold BT.ttf"
            self.stat_font_path = "Template/Fonts/Latin/Belwe Bold BT.ttf"
            self.effect_font_path = "Template/Fonts/Latin/Trade Gothic LT Condensed No. 18.ttf"
            self.effect_bold_font_path = "Template/Fonts/Latin/Trade Gothic LT Bold Condensed No. 20.ttf"
            self.effect_italic_font_path = "Template/Fonts/Latin/Trade Gothic LT Condensed No. 18 Oblique.ttf"
            self.flavour_font_path = "Template/Fonts/Latin/Trade Gothic LT Light Oblique.ttf"

        # Prepare card layer #
        card.image = Image.new("RGBA", (750, 1050), 0)
        # Retrieve a draw handle to write text #
        card.draw = ImageDraw.Draw(card.image)
        # Draw card #
        card.image.paste(self.border_background)

        # Draw art #
        if not card.art:
            try:
                art = Image.open(card.art_path)
            except Exception:
                print("Could not find art file")
                art = Image.open("Template/Dummy.png")
                card.art_path = "Template/Dummy.png"
        else:
            art = card.art
        if card.art_box:
            if len(card.art_box) != 4:
                # Fix for resize function #
                card.art_box = None
        else:
            # Fix for resize function #
            card.art_box = None

        # Change - to — #
        card.flavour = card.flavour.replace("--", "—")
        card.subtypes = card.subtypes.replace("--", "—")
        card.tags = card.tags.replace("--", "—")

        card_type = card.card_type

        if card.variant != "Extended Art" and "Back" not in card.variant:
            # Resize to more or less 1.08 width / height ratio (better have a smaller ratio to avoid white space) #
            art = art.resize((690, 565), 1, card.art_box)
            card.image.paste(art, (30, 100))
        else:
            # Resize to full card ratio #
            art = art.resize((690, 990), 1, card.art_box)
            card.image.paste(art, (30, 30))

        if card.variant == "Extended Art":
            self.drawExtendedArt(card)
        else:
            if card_type == "Ability":
                self.drawAbility(card)
            elif card_type == "Ally":
                self.drawAlly(card)
            elif card_type == "Equipment":
                self.drawEquipment(card)
            elif card_type == "Hero":
                self.drawHero(card)
            elif card_type == "Master Hero":
                self.drawMasterHero(card)
            elif card_type == "Location":
                self.drawLocation(card)
            elif card_type in ["Quest", "Event"]:
                self.drawQuest(card)

        # Draw artist and copyright #
        if card.variant != "Back":
            if card.artist:
                self.drawArtist(card)
            if card.copyright:
                self.drawCopyright(card)
            # Set icon and text #
            self.drawSet(card)

        # Card border #
        if card.border_sharp:
            _, _, _, alpha = self.border_sharp.split()
            gray = ImageOps.grayscale(self.border_sharp)
            result = ImageOps.colorize(gray, card.border_color, (0, 0, 0, 0)) 
            result.putalpha(alpha)
        else:
            _, _, _, alpha = self.border.split()
            gray = ImageOps.grayscale(self.border)
            result = ImageOps.colorize(gray, card.border_color, (0, 0, 0, 0)) 
            result.putalpha(alpha)

        card.image.alpha_composite(result)
        card.image.alpha_composite(self.border_foreground)

        # Instant #
        if card.instant:
            if not card.variant == "Extended Art" and card.card_type not in ["Hero", "Master Hero", "Quest", "Location", "Event"]:
                self.drawInstant(card)
            else:
                pass

        # Draw Basic if needed #
        elif card_type == "Ability":
            card.image.alpha_composite(self.ability_basic, (14, 669))

        if bleeding[0] > 750 or bleeding[1] > 1050:
            to_bleed = card.image
            x_bleed = max(750, bleeding[0])
            y_bleed = max(1050, bleeding[1])
            card.image = Image.new("RGBA", (x_bleed, y_bleed), card.border_color)
            card.image.alpha_composite(to_bleed, (floor((x_bleed - 750) / 2), floor((y_bleed - 1050) / 2)))

        # Draw card end #

        return card.image

    def drawName(self, card, font_size, min_width, max_width, height):
        font = font_size
        font_size_computed = False
        name_box_width = max_width - min_width

        while not font_size_computed:
            card_name_font = ImageFont.truetype(self.main_font_path, font)
            width, _ = card.draw.textsize(card.name, font=card_name_font)

            if width < name_box_width:
                font_size_computed = True
            else:
                font -= 1

        card.draw.text((min_width, height + (font_size - font) / 2), card.name, font=card_name_font, fill=(255, 255, 255, 255), stroke_width=4, stroke_fill=(0, 0, 0, 255))

    def drawCost(self, card, font_size, width, height):
        if card.cost:
            cost_font = ImageFont.truetype(self.stat_font_path, font_size)
            x_offset = card.draw.textsize(card.cost, font=cost_font)[0] / 2
            card.draw.text((width - x_offset, height), card.cost, font=cost_font, fill=(255, 255, 255, 255), stroke_width=4, stroke_fill=(0, 0, 0, 255))

    def drawDamage(self, card, font_size, width, height):
        damage_type = card.damage_type
        damage = card.damage
        image = card.image
        if not damage_type or not damage:
            return

        damage_font = ImageFont.truetype(self.stat_font_path, font_size)
        # Damage Icons #
        if damage_type == "Arcane":
            image.alpha_composite(self.arcane_icon, (11, 888))
        elif damage_type == "Chaos":
            image.alpha_composite(self.chaos_icon, (0, 880))
        elif damage_type == "Fire":
            image.alpha_composite(self.fire_icon, (27, 880))
        elif damage_type == "Frost":
            image.alpha_composite(self.frost_icon, (27, 898))
        elif damage_type == "Holy":
            image.alpha_composite(self.holy_icon, (29, 882))
        elif damage_type == "Melee":
            image.alpha_composite(self.melee_icon, (28, 885))
        elif damage_type == "Nature":
            image.alpha_composite(self.nature_icon, (29, 878))
        elif damage_type == "Ranged":
            image.alpha_composite(self.ranged_icon, (28, 890))
        elif damage_type == "Shadow":
            image.alpha_composite(self.shadow_icon, (28, 884))

        if damage:
            x_offset = card.draw.textsize(damage, font=damage_font)[0] / 2
            card.draw.text((width - x_offset, height), damage, font=damage_font, fill=(255, 255, 255, 255), stroke_width=4, stroke_fill=(0, 0, 0, 255))

    def drawHealth(self, card, font_size, width, height):
        if card.health:
            hp_font = ImageFont.truetype(self.stat_font_path, font_size)
            x_offset = card.draw.textsize(card.health, font=hp_font)[0] / 2
            card.draw.text((width - x_offset, height), card.health, font=hp_font, fill=(255, 255, 255, 255), stroke_width=4, stroke_fill=(0, 0, 0, 255))

    def drawFaction(self, card, min_width=650):
        faction = card.faction

        if faction:
            if faction == "Alliance":
                card.image.alpha_composite(self.alliance_banner, (min_width, 30))
            elif faction == "Horde":
                card.image.alpha_composite(self.horde_banner, (min_width, 30))
            elif faction == "Both":
                card.image.alpha_composite(self.alliance_banner, (min_width - 50, 30))
                card.image.alpha_composite(self.horde_banner, (min_width, 30))
            elif faction == "Monster":
                card.image.alpha_composite(self.monster_banner, (min_width, 30))
            elif faction == "Scourge":
                card.image.alpha_composite(self.scourge_banner, (min_width, 30))
            # Custom faction
            #else:
            #    card.image.alpha_composite(Image.open(faction), (600, 30))

    def parseEffect(self, card):
        if card.effect == "":
            return []

        split_effect = list(filter(None, re.split(r"( |\\|\.|,|<.*?>|\[.*?\]|;|\n)", card.effect)))
        parsed_effect = []
        font_mode = "Standard"
        centered = False

        # Parsed word is [text, font, centered, size(used in formatting and writting)] #

        for word in split_effect:
            if word == "<b>":
                font_mode = "Bold"
            elif word == "<i>":
                font_mode = "Italic"
            elif word == "</b>" and font_mode == "Bold":
                font_mode = "Standard"
            elif word == "</i>" and font_mode == "Italic":
                font_mode = "Standard"
            elif word == "<c>":
                centered = True
            elif word == ";":
                centered = False
                parsed_effect.append(["", "Newblock", False, 0])
            elif word in ["\n", "\\"]:
                if parsed_effect and parsed_effect[-1] == ["", "Newline", False, 0]:
                    parsed_effect[-1] = ["", "Newblock", False, 0]
                else:
                    centered = False
                    parsed_effect.append(["", "Newline", False, 0])
            elif word[0] == "[" and word[-1] == "]":
                parsed_effect.append([word[1:-1], "Icon", centered, 0])
            else:
                parsed_effect.append([word, font_mode, centered, 0])

        return parsed_effect

    def formatEffect(self, card, parsed_effect, font_size=38, min_width=46, max_width=704, max_height=220 ,offset=0):
        final_height = max_height - offset
        if parsed_effect == []:
            return [], 0

        font_size_computed = False
        line_width = max_width - min_width

        while not font_size_computed:
            formatted_effect = []
            current_line = []
            width = 0

            new_lines = 0
            new_blocks = 0

            effect_standard_font = ImageFont.truetype(self.effect_font_path, font_size)
            effect_bold_font = ImageFont.truetype(self.effect_bold_font_path, font_size)
            effect_italic_font = ImageFont.truetype(self.effect_italic_font_path, font_size)

            for word in parsed_effect:
                if word[1] == "Newblock" or word[1] == "Newline":
                    if word[1] == "Newblock":
                        new_blocks += 1
                    else:
                        new_lines += 1
                    word[3] = width
                    current_line.append(word)
                    formatted_effect.append(current_line)
                    current_line = []
                    width = 0
                    continue

                if word[1] == "Bold":
                    word_font = effect_bold_font
                elif word[1] == "Italic":
                    word_font = effect_italic_font
                else:
                    word_font = effect_standard_font

                text = word[0]
                # Handle icons #
                if word[1] == "Icon":
                    if word[0] == "Basic":
                        text = "              "
                    elif word[0] == "Exhaust":
                        text = "     "
                    elif word[0] == "Power":
                        text = "        "
                    elif word[0] == "!Power":
                        text = "        "
                    elif word[0] == "Arcane":
                        text = "     "
                    elif word[0] == "Chaos":
                        text = "     "
                    elif word[0] == "Fire":
                        text = "     "
                    elif word[0] == "Frost":
                        text = "     "
                    elif word[0] == "Holy":
                        text = "     "
                    elif word[0] == "Melee":
                        text = "     "
                    elif word[0] == "Nature":
                        text = "     "
                    elif word[0] == "Ranged":
                        text = "     "
                    elif word[0] == "Shadow":
                        text = "    "
                    elif word[0] == "Health":
                        text = "   "
                    elif word[0] == "Armor":
                        text = "     "
                    elif word[0] == "Alliance":
                        text = "    "
                    elif word[0] == "Horde":
                        text = "    "
                    elif word[0] == "Both":
                        text = "    "
                    elif word[0] == "Monster":
                        text = "    "
                    elif word[0] == "Neutral":
                        text = "     "
                    # handle class icons
                    elif word[0] in ["DK", "DH", "DR", "HU", "MA", "MO", "PA", "PR", "RO", "SH", "WK", "WR"]:
                        text = "      "
                    # cost, !cost or dunno
                    else:
                        text = "    "

                word_width = card.draw.textsize(text, font=word_font)[0]

                if width + word_width > line_width:
                    new_lines += 1
                    if text == " ":
                        formatted_effect.append(current_line)
                        current_line = []
                        width = 0
                        word[3] = 0
                    elif text == ".":
                        last_word = current_line.pop(-1)
                        formatted_effect.append(current_line)
                        current_line = [last_word, word]
                        last_word[3] = 0
                        width = card.draw.textsize(last_word[0], font=word_font)[0]
                        word[3] = width
                        width += word_width
                    else:
                        formatted_effect.append(current_line)
                        current_line = [word]
                        width = word_width
                        word[3] = 0

                else:
                    word[3] = width
                    width += word_width
                    current_line.append(word)

            formatted_effect.append(current_line)

            lines = len(formatted_effect)
            height = ceil(font_size * (lines + (new_lines * 4.0/19 + new_blocks * 12.0/19))) + lines # Add lines to fix pixel loss (dunno why but it works)

            if height <= final_height:
                font_size_computed = True

            else:
                font_size -= 1

        card.font_size = font_size

        return formatted_effect, height

    def drawEffect(self, card, formatted_effect, min_width=46, max_width=704, height=735):
        if formatted_effect == []:
            return

        image = card.image
        draw = card.draw
        font_size = card.font_size

        line_width = max_width - min_width

        newline_height = font_size + ceil(font_size * 4/19)
        newblock_diff = font_size + ceil(font_size * 12/19) - newline_height

        effect_standard_font = ImageFont.truetype(self.effect_font_path, font_size)
        effect_bold_font = ImageFont.truetype(self.effect_bold_font_path, font_size)
        effect_italic_font = ImageFont.truetype(self.effect_italic_font_path, font_size)

        ratio = font_size / 32
        height_offset = 0

        for line in formatted_effect:
            center_offset = 0
            center_computed = False

            for word in line:
                if word[1] == "Newblock":
                    height_offset += newblock_diff
                    continue

                if word[1] == "Bold":
                    word_font = effect_bold_font
                elif word[1] == "Italic":
                    word_font = effect_italic_font
                else:
                    word_font = effect_standard_font

                if word[2]:
                    if not center_computed:
                        font_type = line[-1][1]
                        if font_type == "Bold":
                            last_word_font = effect_bold_font
                        elif font_type == "Italic":
                            last_word_font = effect_italic_font
                        else:
                            last_word_font = effect_standard_font

                        center_offset = floor((line_width - line[-1][3] - draw.textsize(line[-1][0], font=last_word_font)[0]) / 2)
                        center_computed = True
                else:
                    center_offset = 0
                    center_computed = False

                text = word[0]
                width_offset = word[3] + center_offset

                if word[1] == "Icon":
                    # ToDo adjust icons #
                    if text == "Basic":
                        text_icon = Image.open("Template/Text Box Icons/Basic.png").resize((int(100 * ratio), int(40 * ratio)), 1)
                        image.alpha_composite(text_icon, (min_width + width_offset, height + height_offset))
                    elif text == "Exhaust":
                        text_icon = Image.open("Template/Text Box Icons/Exhaust.png").resize((int(40 * ratio), int(30 * ratio)), 1)
                        image.alpha_composite(text_icon, (min_width + width_offset - 5, height + height_offset + 5))
                    elif text == "Power":
                        text_icon = Image.open("Template/Text Box Icons/Power.png").resize((int(60 * ratio), int(30 * ratio)), 1)
                        image.alpha_composite(text_icon, (min_width + width_offset, height + height_offset + int(6 * ratio)))
                    elif text == "!Power":
                        text_icon = Image.open("Template/Text Box Icons/Power Invert.png").resize((int(60 * ratio), int(30 * ratio)), 1)
                        image.alpha_composite(text_icon, (min_width + width_offset, height + height_offset + int(6 * ratio)))
                    elif text == "Arcane":
                        text_icon = Image.open("Template/Text Box Icons/Arcane.png").resize((int(38 * ratio), int(38 * ratio)), 1)
                        image.alpha_composite(text_icon, (min_width + width_offset - int(1 * ratio), height + height_offset - int(0 * ratio)))
                    elif text == "Chaos":
                        text_icon = Image.open("Template/Text Box Icons/Chaos.png").resize((int(40 * ratio), int(40 * ratio)), 1)
                        image.alpha_composite(text_icon, (min_width + width_offset - int(0 * ratio), height + height_offset - int(2 * ratio)))
                    elif text == "Fire":
                        text_icon = Image.open("Template/Text Box Icons/Fire.png").resize((int(34 * ratio), int(36 * ratio)), 1)
                        image.alpha_composite(text_icon, (min_width + width_offset + int(3 * ratio), height + height_offset - int(1 * ratio)))
                    elif text == "Frost":
                        text_icon = Image.open("Template/Text Box Icons/Frost.png").resize((int(40 * ratio), int(40 * ratio)), 1)
                        image.alpha_composite(text_icon, (min_width + width_offset - int(0 * ratio), height + height_offset - int(3 * ratio)))
                    elif text == "Holy":
                        text_icon = Image.open("Template/Text Box Icons/Holy.png").resize((int(38 * ratio), int(38 * ratio)), 1)
                        image.alpha_composite(text_icon, (min_width + width_offset - int(1 * ratio), height + height_offset - int(2 * ratio)))
                    elif text == "Melee":
                        text_icon = Image.open("Template/Text Box Icons/Melee.png").resize((int(34 * ratio), int(34 * ratio)), 1)
                        image.alpha_composite(text_icon, (min_width + width_offset + int(2 * ratio), height + height_offset - int(0 * ratio)))
                    elif text == "Nature":
                        text_icon = Image.open("Template/Text Box Icons/Nature.png").resize((int(36 * ratio), int(36 * ratio)), 1)
                        image.alpha_composite(text_icon, (min_width + width_offset - int(0 * ratio), height + height_offset - int(0 * ratio)))
                    elif text == "Ranged":
                        text_icon = Image.open("Template/Text Box Icons/Ranged.png").resize((int(36 * ratio), int(36 * ratio)), 1)
                        image.alpha_composite(text_icon, (min_width + width_offset + int(1 * ratio), height + height_offset - int(0 * ratio)))
                    elif text == "Shadow":
                        text_icon = Image.open("Template/Text Box Icons/Shadow.png").resize((int(36 * ratio), int(36 * ratio)), 1)
                        image.alpha_composite(text_icon, (min_width + width_offset - int(3 * ratio), height + height_offset - int(0 * ratio)))
                    elif text == "Health":
                        text_icon = Image.open("Template/Text Box Icons/Health.png").resize((int(36 * ratio), int(36 * ratio)), 1)
                        image.alpha_composite(text_icon, (min_width + width_offset - int(5 * ratio), height + height_offset + int(0 * ratio)))
                    elif text == "Armor":
                        text_icon = Image.open("Template/Text Box Icons/Armor.png").resize((int(36 * ratio), int(36 * ratio)), 1)
                        image.alpha_composite(text_icon, (min_width + width_offset + int(1 * ratio), height + height_offset + int(0 * ratio)))
                    elif text == "Alliance":
                        text_icon = Image.open("Template/Text Box Icons/Alliance.png").resize((int(25 * ratio), int(36 * ratio)), 1)
                        image.alpha_composite(text_icon, (min_width + width_offset + int(1 * ratio), height + height_offset + int(0 * ratio)))
                    elif text == "Horde":
                        text_icon = Image.open("Template/Text Box Icons/Horde.png").resize((int(25 * ratio), int(36 * ratio)), 1)
                        image.alpha_composite(text_icon, (min_width + width_offset + int(1 * ratio), height + height_offset + int(0 * ratio)))
                    elif text == "Both":
                        text_icon = Image.open("Template/Text Box Icons/Both.png").resize((int(25 * ratio), int(36 * ratio)), 1)
                        image.alpha_composite(text_icon, (min_width + width_offset + int(1 * ratio), height + height_offset + int(0 * ratio)))
                    elif text == "Monster":
                        text_icon = Image.open("Template/Text Box Icons/Monster.png").resize((int(28 * ratio), int(36 * ratio)), 1)
                        image.alpha_composite(text_icon, (min_width + width_offset + int(1 * ratio), height + height_offset + int(0 * ratio)))
                    elif text == "Neutral":
                        text_icon = Image.open("Template/Text Box Icons/Neutral.png").resize((int(36 * ratio), int(36 * ratio)), 1)
                        image.alpha_composite(text_icon, (min_width + width_offset + int(1 * ratio), height + height_offset + int(0 * ratio)))
                    elif text == "Scourge":
                        text_icon = Image.open("Template/Text Box Icons/Scourge.png").resize((int(25 * ratio), int(36 * ratio)), 1)
                        image.alpha_composite(text_icon, (min_width + width_offset + int(1 * ratio), height + height_offset + int(0 * ratio)))
                    elif text == "DK":
                        text_icon = Image.open("Template/Class Icons/Death Knight.png").resize((int(36 * ratio), int(36 * ratio)), 1)
                        image.alpha_composite(text_icon, (min_width + width_offset + int(1 * ratio), height + height_offset + int(3 * ratio)))
                    elif text == "DH":
                        text_icon = Image.open("Template/Class Icons/Demon Hunter.png").resize((int(36 * ratio), int(36 * ratio)), 1)
                        image.alpha_composite(text_icon, (min_width + width_offset + int(1 * ratio), height + height_offset + int(3 * ratio)))
                    elif text == "DR":
                        text_icon = Image.open("Template/Class Icons/Druid.png").resize((int(36 * ratio), int(36 * ratio)), 1)
                        image.alpha_composite(text_icon, (min_width + width_offset + int(1 * ratio), height + height_offset + int(3 * ratio)))
                    elif text == "HU":
                        text_icon = Image.open("Template/Class Icons/Hunter.png").resize((int(36 * ratio), int(36 * ratio)), 1)
                        image.alpha_composite(text_icon, (min_width + width_offset + int(1 * ratio), height + height_offset + int(3 * ratio)))
                    elif text == "MA":
                        text_icon = Image.open("Template/Class Icons/Mage.png").resize((int(36 * ratio), int(36 * ratio)), 1)
                        image.alpha_composite(text_icon, (min_width + width_offset + int(1 * ratio), height + height_offset + int(3 * ratio)))
                    elif text == "MO":
                        text_icon = Image.open("Template/Class Icons/Monk.png").resize((int(36 * ratio), int(36 * ratio)), 1)
                        image.alpha_composite(text_icon, (min_width + width_offset + int(1 * ratio), height + height_offset + int(3 * ratio)))
                    elif text == "PA":
                        text_icon = Image.open("Template/Class Icons/Paladin.png").resize((int(36 * ratio), int(36 * ratio)), 1)
                        image.alpha_composite(text_icon, (min_width + width_offset + int(1 * ratio), height + height_offset + int(3 * ratio)))
                    elif text == "PR":
                        text_icon = Image.open("Template/Class Icons/Priest.png").resize((int(36 * ratio), int(36 * ratio)), 1)
                        image.alpha_composite(text_icon, (min_width + width_offset + int(1 * ratio), height + height_offset + int(3 * ratio)))
                    elif text == "RO":
                        text_icon = Image.open("Template/Class Icons/Rogue.png").resize((int(36 * ratio), int(36 * ratio)), 1)
                        image.alpha_composite(text_icon, (min_width + width_offset + int(1 * ratio), height + height_offset + int(3 * ratio)))
                    elif text == "SH":
                        text_icon = Image.open("Template/Class Icons/Shaman.png").resize((int(36 * ratio), int(36 * ratio)), 1)
                        image.alpha_composite(text_icon, (min_width + width_offset + int(1 * ratio), height + height_offset + int(3 * ratio)))
                    elif text == "WK":
                        text_icon = Image.open("Template/Class Icons/Warlock.png").resize((int(36 * ratio), int(36 * ratio)), 1)
                        image.alpha_composite(text_icon, (min_width + width_offset + int(1 * ratio), height + height_offset + int(3 * ratio)))
                    elif text == "WR":
                        text_icon = Image.open("Template/Class Icons/Warrior.png").resize((int(36 * ratio), int(36 * ratio)), 1)
                        image.alpha_composite(text_icon, (min_width + width_offset + int(1 * ratio), height + height_offset + int(3 * ratio)))

                    # cost, !cost or dunno

                    elif len(text):
                        if text[0] != "!":
                            text_numeric_cost = Image.open("Template/Text Box Icons/Numeric Cost.png").resize((int(36 * ratio), int(36 * ratio)), 1)
                            cost_font = ImageFont.truetype(self.cost_font_path, font_size - 4)

                            # Handle plural digits #
                            text_len = len(text)
                            size_offset = 0
                            if text_len > 1:
                                size_offset -= draw.textsize(text, font=cost_font)[0] / text_len * (text_len - 1) / 2

                            image.alpha_composite(text_numeric_cost, (min_width + width_offset - int(5 * ratio), height + height_offset + int(4 * ratio)))
                            draw.text((min_width + width_offset + size_offset + int(8 * ratio), height + height_offset + int(10 * ratio)), text, font=cost_font, fill=(255, 255, 255, 255))

                        else:
                            text = text[1:-1]
                            text_numeric_cost = Image.open("Template/Text Box Icons/Numeric Cost Invert.png").resize((int(36 * ratio), int(36 * ratio)), 1)
                            cost_font = ImageFont.truetype(self.cost_font_path, font_size - 4)

                            # Handle plural digits #
                            text_len = len(text)
                            size_offset = 0
                            if text_len > 1:
                                size_offset -= draw.textsize(text, font=cost_font)[0] / text_len * (text_len - 1) / 2

                            image.alpha_composite(text_numeric_cost, (min_width + width_offset - int(5 * ratio), height + height_offset + int(4 * ratio)))
                            draw.text((min_width + width_offset + size_offset + int(8 * ratio), height + height_offset + int(10 * ratio)), text, font=cost_font, fill=(0, 0, 0, 255))
                    # No text #
                    else:
                        pass

                else:
                    draw.text((min_width + width_offset, height + height_offset), text, font=word_font, fill=(0, 0, 0, 255))

            height_offset += newline_height

    def drawBackground(self, card):
        # Draw text background if any #
        if card.background_path:
            try:
                bg_art = Image.open(card.background_path)
                card.image.alpha_composite(bg_art)
            except Exception:
                print("Could not find background file")

    def formatFlavour(self, card, font_size=20, min_width=146, max_width=604):
        if card.flavour == "":
            return False

        font_size_computed = False
        flavor_width = max_width - min_width
        split_flavour = re.split("(\s+|;|\n+)", card.flavour)
        
        # Sanitize flavour #
        while split_flavour != [] and split_flavour[-1] in ["", " ", ";", "\n"]:
            split_flavour.pop(-1)

        if split_flavour == []:
            return False

        while not font_size_computed:
            flavour_font = ImageFont.truetype(self.flavour_font_path, font_size)

            height = font_size
            lines_list = []
            current_line = ""

            newline_height = font_size + ceil(font_size * 4/19)

            for word in split_flavour:
                if (len(current_line) == 0 and word == " ") or word == "":
                    pass

                elif word == ";" or word == "\n":
                    
                    if current_line and current_line[-1] == " ":
                        current_line = current_line[:-1]

                    lines_list.append(current_line)
                    current_line = ""
                    height += newline_height

                else:
                    current_line += word

            if not current_line:
                current_line = lines_list[-1]

            if current_line[-1] == " ":
                current_line = current_line[:-1]

            lines_list.append(current_line)

            font_size_computed = True
            for line in lines_list:
                if card.draw.textsize(line, flavour_font)[0] > flavor_width:
                    font_size_computed = False
                    font_size -= 1

        card.font_size = font_size
        card.lines_list = lines_list

        return True

    def drawFlavour(self, card, height=900):
        draw = card.draw
        font_size = card.font_size
        # Draw #
        newline_height = font_size + ceil(font_size * 4/19) - 3 # Strange fix

        flavour_font = ImageFont.truetype(self.flavour_font_path, font_size)

        nb_lines = len(card.lines_list)

        height_offset = 22 * (3 - nb_lines)

        for line in card.lines_list:
            line_size = draw.textsize(line, flavour_font)[0]
            target_width = floor(375 - (line_size / 2.0))
            draw.text((target_width, height + height_offset), line, font=flavour_font, fill=(0, 0, 0, 255))
            height_offset += newline_height

        return 22 * nb_lines

    def drawTypeTags(self, card, font_size=25, min_width=89, max_width=682, height=670):
        font_size_computed = False
        font = font_size
        width = max_width - min_width

        draw = card.draw

        # Type #
        if card.type_override:
            text = card.type_override
        else:
            text = card.locale["type_names"][self.type_names.index(card.card_type)]
            if card.card_type in ["Ally", "Hero"]:
                if card.faction == "Monster":
                    text = card.locale["faction_names"][-2] + " " + text
                elif card.faction == "Scourge":
                    text = card.locale["faction_names"][-1] + " " + text
        if card.subtypes != "":
            text += " — " + card.subtypes

        type_str = text
        text += "    "

        # Tag #
        if card.tags != "":
            text += card.tags

        while not font_size_computed:
            card_type_font = ImageFont.truetype(self.main_font_path, font)
            x_size, _ = draw.textsize(text, font=card_type_font)

            if x_size > width:
                font -= 1

            else:
                font_size_computed = True

        y_offset = int((25 - font) / 2)
        draw.text((min_width, height + y_offset), type_str, font=card_type_font, fill=(255, 255, 255, 255), stroke_width=3, stroke_fill=(0, 0, 0, 255))

        if card.tags:
            x_offset, _ = draw.textsize(card.tags, font=card_type_font)
            draw.text((max_width - x_offset, height + y_offset), card.tags, font=card_type_font, fill=(255, 255, 255, 255), stroke_width=3, stroke_fill=(0, 0, 0, 255))

    def drawClasses(self, card, width=642, height=37):
        image = card.image
        class_icons = card.class_icons

        icon_offset = width
        for class_name in reversed(sorted(class_icons)):
            if icon_offset < 0:
                break
            if class_name == "Warrior":
                image.alpha_composite(self.warrior_icon, (icon_offset, height))
                icon_offset -= 72
            elif class_name == "Warlock":
                image.alpha_composite(self.warlock_icon, (icon_offset, height))
                icon_offset -= 72
            elif class_name == "Shaman":
                image.alpha_composite(self.shaman_icon, (icon_offset, height))
                icon_offset -= 72
            elif class_name == "Rogue":
                image.alpha_composite(self.rogue_icon, (icon_offset, height))
                icon_offset -= 72
            elif class_name == "Priest":
                image.alpha_composite(self.priest_icon, (icon_offset, height))
                icon_offset -= 72
            elif class_name == "Paladin":
                image.alpha_composite(self.paladin_icon, (icon_offset, height))
                icon_offset -= 72
            elif class_name == "Monk Alt":
                image.alpha_composite(self.monk_alt_icon, (icon_offset - 3, height - 1))
                icon_offset -= 72
            elif class_name == "Monk":
                image.alpha_composite(self.monk_icon, (icon_offset, height))
                icon_offset -= 72
            elif class_name == "Mage":
                image.alpha_composite(self.mage_icon, (icon_offset, height))
                icon_offset -= 72
            elif class_name == "Hunter":
                image.alpha_composite(self.hunter_icon, (icon_offset, height))
                icon_offset -= 72
            elif class_name == "Druid":
                image.alpha_composite(self.druid_icon, (icon_offset, height))
                icon_offset -= 72
            elif class_name == "Demon Hunter":
                image.alpha_composite(self.demon_hunter_icon, (icon_offset, height))
                icon_offset -= 72
            elif class_name == "Death Knight":
                image.alpha_composite(self.death_knight_icon, (icon_offset, height))
                icon_offset -= 72

    def drawArtist(self, card):
        artist_font = ImageFont.truetype(self.artist_copyright_font_path, 18)
        card.draw.text((404, 978), card.locale["artist"] + ": " + card.artist, font=artist_font, fill=(255, 255, 255, 255), stroke_width=1, stroke_fill=(0, 0, 0, 255))

    def drawCopyright(self, card):
        copyright_font = ImageFont.truetype(self.artist_copyright_font_path, 13)
        card.draw.text((404, 998), card.copyright, font=copyright_font, fill=(255, 255, 255, 255), stroke_width=1, stroke_fill=(0, 0, 0, 255))

    def drawSet(self, card):
        set_text = ""
        if "token" in card.type_override.lower()and (not card.set_number or not card.set_max_number):
                set_text = " " + card.set_name + " TOKEN"
        else:
            if card.set_name:
                set_text = " " + card.set_name
                if card.set_number and card.set_max_number:
                    set_text += " " + card.set_number + " / " + card.set_max_number

        base_font = 20
        font = 20
        font_size_computed = False
        set_box_width = 175

        while not font_size_computed:
            set_font = ImageFont.truetype(self.set_font_path, font)
            set_text_width, _ = card.draw.textsize(set_text, font=set_font)

            if set_text_width < set_box_width:
                font_size_computed = True
            else:
                font -= 1

        digits_offset = len(card.block_number) - 1
        block_font = ImageFont.truetype(self.set_font_path, 20 - 2 * digits_offset)
        block_number_width, _ = card.draw.textsize(card.block_number, font=set_font)

        rarity_stroke = (255, 255, 255, 255) # Same as the block number color #

        if card.rarity == "Epic":
            rarity_color = self.epic_color
            set_icon = self.set_icon_epic
        elif card.rarity == "Rare":
            rarity_color = self.rare_color
            set_icon = self.set_icon_rare
        elif card.rarity == "Uncommon":
            rarity_color = self.uncommon_color
            set_icon = self.set_icon_uncommon
        else:
            rarity_color = self.common_color
            set_icon = self.set_icon_common
            rarity_stroke = (0, 0, 0, 255)

        card.image.alpha_composite(set_icon, (335 - set_text_width, 980))
        card.draw.text((355 - set_text_width - block_number_width / 2 + 2 * digits_offset, 980 + digits_offset), card.block_number, font=block_font, fill=rarity_stroke)
        card.draw.text((375 - set_text_width, 982 + (base_font - font) / 2), set_text, font=set_font, fill=rarity_color, stroke_width=2, stroke_fill=rarity_stroke)

    def drawInstant(self, card, height=668):
        card.image.alpha_composite(self.instant_border, (15, 26))
        card.image.alpha_composite(self.instant_plate, (15, height))

    def drawAbility(self, card):
        # Frame #
        if card.variant not in ["None", "Extended Art"] and exists("Template/Custom/Ability/" + card.variant):
            card.image.alpha_composite(Image.open("Template/Custom/Ability/" + card.variant).resize((690, 990), 1), (30, 30))
        else:
            card.image.alpha_composite(self.ability_frame, (30, 30))
        # Class icons #
        self.drawClasses(card)
        # Name #
        self.drawName(card, 38, 187, 635, 36)
        # Cost #
        self.drawCost(card, 55, 98, 51)
        # Faction #
        if len(card.class_icons) == 0:
            self.drawFaction(card)
        # Types and tags #
        self.drawTypeTags(card, min_width=135)
        # Background #
        self.drawBackground(card)
        # Flavour #
        offset = 0
        if self.formatFlavour(card):
            offset = self.drawFlavour(card)
        # Effect #
        effect = self.parseEffect(card)
        effect, _ = self.formatEffect(card, effect, max_height=220 - offset)
        self.drawEffect(card, effect)

    def drawAlly(self, card):
        if card.variant not in ["None", "Extended Art"] and exists("Template/Custom/Ally/" + card.variant):
            card.image.alpha_composite(Image.open("Template/Custom/Ally/" + card.variant).resize((690, 990), 1), (30, 30))
            # Name #
            self.drawName(card, 38, 223, 635, 36)
            # Cost #
            self.drawCost(card, 55, 102, 47)
            # Faction #
            self.drawFaction(card)

        elif card.faction in ["Neutral", "Both"]:
            # Frame #
            card.image.alpha_composite(self.ally_neutral_frame, (30, 30))
            # Name #
            self.drawName(card, 38, 223, 635, 36)
            # Cost #
            self.drawCost(card, 55, 102, 47)
            # Faction #
            self.drawFaction(card)

        else:
            if card.faction == "Alliance":
                # Frame #
                card.image.alpha_composite(self.ally_alliance_frame, (30, 30))
                # Cost #
                self.drawCost(card, 55, 126, 41)

            elif card.faction == "Horde":
                # Frame #
                card.image.alpha_composite(self.ally_horde_frame, (30, 30))
                # Cost #
                self.drawCost(card, 55, 100, 45)

            elif card.faction == "Monster":
                # Frame #
                card.image.alpha_composite(self.ally_monster_frame, (30, 30))
                # Cost #
                self.drawCost(card, 55, 102, 53)
            elif card.faction == "Scourge":
                # Frame #
                card.image.alpha_composite(self.ally_scourge_frame, (30, 30))
                # Cost #
                self.drawCost(card, 55, 104, 46)

            # Name #
            self.drawName(card, 38, 200, 635, 36)

        # Types and tags #
        if card.instant:
            self.drawTypeTags(card, min_width=135)
        else:
            self.drawTypeTags(card)
        # Damage #
        self.drawDamage(card, 60, 77, 923)
        # Health #
        self.drawHealth(card, 60, 665, 923)
        # Class icons #
        if card.faction in ["Monster", "Neutral"]:
            self.drawClasses(card)
        # Background #
        self.drawBackground(card)
        # Flavour #
        offset = 0
        if self.formatFlavour(card):
            offset = self.drawFlavour(card)
        # Effect #
        effect = self.parseEffect(card)
        effect, _ = self.formatEffect(card, effect, offset = offset)
        self.drawEffect(card, effect)

    def drawEquipment(self, card):
        image = card.image
        class_icons = card.class_icons

        if card.variant in ["Low", "Low Side Bar"]:
            # Frame #
            image.alpha_composite(self.equipment_lowsidebar_frame, (30, 30))
            # Class icons #
            if "Death Knight" in class_icons or class_icons == ["All"]:
                image.alpha_composite(self.equipment_class_death_knight, (646, 131))
            if "Druid" in class_icons or class_icons == ["All"]:
                image.alpha_composite(self.equipment_class_druid, (646, 182))
            if "Hunter" in class_icons or class_icons == ["All"]:
                image.alpha_composite(self.equipment_class_hunter, (646, 233))
            if "Mage" in class_icons or class_icons == ["All"]:
                image.alpha_composite(self.equipment_class_mage, (646, 284))
            if "Paladin" in class_icons or class_icons == ["All"]:
                image.alpha_composite(self.equipment_class_paladin, (646, 334))
            if "Priest" in class_icons or class_icons == ["All"]:
                image.alpha_composite(self.equipment_class_priest, (646, 385))
            if "Rogue" in class_icons or class_icons == ["All"]:
                image.alpha_composite(self.equipment_class_rogue, (646, 436))
            if "Shaman" in class_icons or class_icons == ["All"]:
                image.alpha_composite(self.equipment_class_shaman, (646, 486))
            if "Warlock" in class_icons or class_icons == ["All"]:
                image.alpha_composite(self.equipment_class_warlock, (646, 537))
            if "Warrior" in class_icons or class_icons == ["All"]:
                image.alpha_composite(self.equipment_class_warrior, (646, 588))

            # Faction #
            self.drawFaction(card)

        else:
            if card.variant in ["No", "No Side Bar"]:
                # Frame #
                image.alpha_composite(self.equipment_nosidebar_frame)

                # Faction #
                self.drawFaction(card)

            else:   # Default equipment type is High #
                # Frame #
                image.alpha_composite(self.equipment_highsidebar_frame, (30, 30))
                # Background class icons #
                image.alpha_composite(self.equipment_background_classes, (653, 35))

                # Faction #
                self.drawFaction(card, 550)

            # Class Icons #
            if "Death Knight" in class_icons or class_icons == ["All"]:
                image.alpha_composite(self.equipment_class_death_knight, (646, 35))
            if "Demon Hunter" in class_icons or class_icons == ["All"]:
                image.alpha_composite(self.equipment_class_demon_hunter, (646, 87))
            if "Druid" in class_icons or class_icons == ["All"]:
                image.alpha_composite(self.equipment_class_druid, (646, 139))
            if "Hunter" in class_icons or class_icons == ["All"]:
                image.alpha_composite(self.equipment_class_hunter, (646, 191))
            if "Mage" in class_icons or class_icons == ["All"]:
                image.alpha_composite(self.equipment_class_mage, (646, 244))
            if "Monk" in class_icons or class_icons == ["All"]:
                image.alpha_composite(self.equipment_class_monk_alt, (646, 294))
            if "Paladin" in class_icons or class_icons == ["All"]:
                image.alpha_composite(self.equipment_class_paladin, (646, 346))
            if "Priest" in class_icons or class_icons == ["All"]:
                image.alpha_composite(self.equipment_class_priest, (646, 397))
            if "Rogue" in class_icons or class_icons == ["All"]:
                image.alpha_composite(self.equipment_class_rogue, (646, 448))
            if "Shaman" in class_icons or class_icons == ["All"]:
                image.alpha_composite(self.equipment_class_shaman, (646, 500))
            if "Warlock" in class_icons or class_icons == ["All"]:
                image.alpha_composite(self.equipment_class_warlock, (646, 552))
            if "Warrior" in class_icons or class_icons == ["All"]:
                image.alpha_composite(self.equipment_class_warrior, (646, 604))

        # Name #
        name_offset = 0
        if card.variant == "High Side Bar":
            if card.faction == "Both":
                name_offset = 130
            elif card.faction in ["Alliance", "Horde"]:
                name_offset = 65
            elif card.faction == "Monster":
                name_offset = 75

        self.drawName(card, 38, 205, 620 - name_offset, 36)
        # Cost #
        self.drawCost(card, 55, 100, 47)
        # Types and tags #
        if card.instant:
            self.drawTypeTags(card, min_width=135)
        else:    
            self.drawTypeTags(card)

        height = 220

        # Equipment Ally #
        if card.health:
            image.alpha_composite(self.equipment_health_icon, (578, 890))
            self.drawHealth(card, 60, 665, 923)

        # Weapon #
        elif card.strike_cost:
            strike_font = ImageFont.truetype(self.stat_font_path, 60)
            image.alpha_composite(self.equipment_strike_icon, (605, 902))
            x_offset = card.draw.textsize(card.strike_cost, font=strike_font)[0] / 2
            card.draw.text((665 - x_offset, 923), card.strike_cost, font=strike_font, fill=(255, 255, 255, 255), stroke_width=4, stroke_fill=(0, 0, 0, 255))

        # Armor #
        elif card.armor:
            height = 150
            armor_font = ImageFont.truetype(self.stat_font_path, 60)
            image.alpha_composite(self.equipment_armor_icon, (607, 901))
            x_offset = card.draw.textsize(card.armor, font=armor_font)[0] / 2
            card.draw.text((665 - x_offset, 923), card.armor, font=armor_font, fill=(255, 255, 255, 255), stroke_width=4, stroke_fill=(0, 0, 0, 255))

        if card.strike_cost or card.health:
            height = 150
            self.drawDamage(card, 60, 77, 923)

        # Background #
        self.drawBackground(card)
        # Flavour #
        offset = 0
        if self.formatFlavour(card):
            offset = self.drawFlavour(card)
        # Effect #
        effect = self.parseEffect(card)
        effect, _ = self.formatEffect(card, effect, offset = offset)
        self.drawEffect(card, effect)

    def drawHero(self, card):
        image = card.image
        class_icons = card.class_icons

        if "Back" in card.variant:
            offset = 0

            if card.effect:
                # Compute effect layout and y offset #
                effect = self.parseEffect(card)
                effect, height = self.formatEffect(card, effect, 21, 90, 640, 1000)
                offset = height - 21

                # Text background #
                image.alpha_composite(self.hero_back_text_background, (30, 952 - offset))

            if card.variant != "Back" and exists("Template/Custom/Hero/" + card.variant):
                image.alpha_composite(Image.open("Template/Custom/Hero/" + card.variant).resize((690, 149), 1), (30, 843 - offset))
            else:
                # Frame fix #
                image.alpha_composite(self.hero_back_fix, (30, 843 - offset))
                if card.faction == "Alliance":
                    image.alpha_composite(self.hero_back_alliance, (30, 843 - offset))
                elif card.faction == "Horde":
                    image.alpha_composite(self.hero_back_horde, (30, 843 - offset))
                elif card.faction in ["Neutral", "Both"]:
                    image.alpha_composite(self.hero_back_neutral, (30, 843 - offset))
                    if card.faction == "Both":
                        image.alpha_composite(self.hero_back_both_flag, (584, 843 - offset))
                elif card.faction == "Scourge":
                    image.alpha_composite(self.hero_back_scourge, (30, 843 - offset))
                else:   # Default front hero faction is Monster #
                    image.alpha_composite(self.hero_back_monster, (30, 843 - offset))

            # Class icons #
            if len(class_icons) > 1:
                self.drawClasses(card, 115, 858 - offset)
            else:
                if "Death Knight" in class_icons:
                    image.alpha_composite(self.hero_back_death_knight_icon, (73, 852 - offset))
                if "Demon Hunter" in class_icons:
                    image.alpha_composite(self.hero_back_demon_hunter_icon, (73, 852 - offset))
                if "Druid" in class_icons:
                    image.alpha_composite(self.hero_back_druid_icon, (73, 852 - offset))
                if "Hunter" in class_icons:
                    image.alpha_composite(self.hero_back_hunter_icon, (73, 852 - offset))
                if "Mage" in class_icons:
                    image.alpha_composite(self.hero_back_mage_icon, (73, 852 - offset))
                if "Monk" in class_icons:
                    image.alpha_composite(self.hero_back_monk_icon, (73, 852 - offset))
                if "Monk Alt" in class_icons:
                    image.alpha_composite(self.hero_back_monk_alt_icon, (70, 852 - offset))
                if "Paladin" in class_icons:
                    image.alpha_composite(self.hero_back_paladin_icon, (73, 852 - offset))
                if "Priest" in class_icons:
                    image.alpha_composite(self.hero_back_priest_icon, (73, 852 - offset))
                if "Rogue" in class_icons:
                    image.alpha_composite(self.hero_back_rogue_icon, (73, 852 - offset))
                if "Shaman" in class_icons:
                    image.alpha_composite(self.hero_back_shaman_icon, (73, 852 - offset))
                if "Warlock" in class_icons:
                    image.alpha_composite(self.hero_back_warlock_icon, (73, 852 - offset))
                if "Warrior" in class_icons:
                    image.alpha_composite(self.hero_back_warrior_icon, (73, 852 - offset))

            # Name #
            self.drawName(card, 25, 310, 550, 905 - offset)
            # Health #
            self.drawHealth(card, 55, 213, 856 - offset)
            # Effect #
            if card.effect:
                self.drawEffect(card, effect, 104, 640, 975 - offset)

        else:   # Default hero variant is front #
            if card.variant not in ["Front", "Back"] and exists("Template/Custom/Hero/" + card.variant):
                image.alpha_composite(Image.open("Template/Custom/Hero/" + card.variant).resize((690, 990), 1), (30, 30))
            elif card.faction == "Alliance":
                image.alpha_composite(self.hero_alliance, (30, 30))
            elif card.faction == "Horde":
                image.alpha_composite(self.hero_horde, (30, 30))
            elif card.faction in ["Neutral", "Both"]:
                image.alpha_composite(self.hero_neutral, (30, 30))
                if card.faction == "Both":
                    image.alpha_composite(self.hero_both_flag, (35, 29))
            elif card.faction == "Scourge":
                image.alpha_composite(self.hero_scourge, (30, 30))
            else:   # Default front hero faction is Monster #
                image.alpha_composite(self.hero_monster, (30, 30))

            # Class icons #

            # Check if multiple class hero #
            if len(class_icons) > 1:
                self.drawClasses(card)
            else:
                if "Death Knight" in class_icons:
                    image.alpha_composite(self.hero_death_knight_icon, (591, 39))
                if "Demon Hunter" in class_icons:
                    image.alpha_composite(self.hero_demon_hunter_icon, (589, 38))
                if "Druid" in class_icons:
                    image.alpha_composite(self.hero_druid_icon, (591, 39))
                if "Hunter" in class_icons:
                    image.alpha_composite(self.hero_hunter_icon, (591, 39))
                if "Mage" in class_icons:
                    image.alpha_composite(self.hero_mage_icon, (591, 39))
                if "Monk" in class_icons:
                    image.alpha_composite(self.hero_monk_icon, (591, 39))
                if "Monk Alt" in class_icons:
                    image.alpha_composite(self.hero_monk_alt_icon, (584, 38))
                if "Paladin" in class_icons:
                    image.alpha_composite(self.hero_paladin_icon, (591, 39))
                if "Priest" in class_icons:
                    image.alpha_composite(self.hero_priest_icon, (591, 39))
                if "Rogue" in class_icons:
                    image.alpha_composite(self.hero_rogue_icon, (591, 39))
                if "Shaman" in class_icons:
                    image.alpha_composite(self.hero_shaman_icon, (591, 39))
                if "Warlock" in class_icons:
                    image.alpha_composite(self.hero_warlock_icon, (591, 39))
                if "Warrior" in class_icons:
                    image.alpha_composite(self.hero_warrior_icon, (591, 39))

            # Name #
            self.drawName(card, 38, 187, 550, 36)
            # Health #
            self.drawHealth(card, 60, 665, 923)
            # Types and tags #
            self.drawTypeTags(card)
            # Background #
            self.drawBackground(card)
            # Flavour #
            offset = 0
            if self.formatFlavour(card):
                offset = self.drawFlavour(card)
            # Effect #
            effect = self.parseEffect(card)
            effect, _ = self.formatEffect(card, effect, offset = offset)
            self.drawEffect(card, effect)

        if card.damage:
            self.drawDamage(card, 60, 77, 923)

    def drawMasterHero(self, card):
        # ? #
        #self.attack_hp_font = ImageFont.truetype('Template/Fonts/Belwe Bold BT.ttf', 60)

        # Frame #
        card.image.alpha_composite(self.master_hero_frame, (30, 30))
        # Class icons #
        self.drawClasses(card)
        # Name #
        self.drawName(card, 38, 187, 635, 36)
        # Cost #
        self.drawCost(card, 55, 98, 51)
        # Faction #
        if len(card.class_icons) == 0:
            self.drawFaction(card)
        # Damage #
        self.drawDamage(card, 60, 77, 923)
        # Health #
        self.drawHealth(card, 60, 665, 923)
        # Types and tags #
        self.drawTypeTags(card, min_width=135)
        # Background #
        self.drawBackground(card)
        # Flavour #
        offset = 0
        if self.formatFlavour(card):
            offset = self.drawFlavour(card)
        # Effect #
        effect = self.parseEffect(card)
        effect, _ = self.formatEffect(card, effect, max_height=220 - offset)
        self.drawEffect(card, effect)

    def drawLocation(self, card):
        # Frame #
        if card.variant not in ["None", "Extended Art"] and exists("Template/Custom/Location/" + card.variant):
            card.image.alpha_composite(Image.open("Template/Custom/Location/" + card.variant).resize((690, 990), 1), (30, 30))
        else:
            card.image.alpha_composite(self.location_frame, (30, 30))

        # Name #
        self.drawName(card, 38, 155, 550, 36)
        # Types and tags #
        self.drawTypeTags(card)
        # Faction #
        self.drawFaction(card)

        if card.capacity:
            card.image.alpha_composite(self.location_capacity, (605, 905))
            capacity_font = ImageFont.truetype(self.stat_font_path, 60)
            x_offset = card.draw.textsize(card.capacity, font=capacity_font)[0] / 2
            card.draw.text((658 - x_offset, 923), card.capacity, font=capacity_font, fill=(255, 255, 255, 255), stroke_width=4, stroke_fill=(0, 0, 0, 255))

        # Background #
        self.drawBackground(card)
        # Flavour #
        offset = 0
        if self.formatFlavour(card):
            offset = self.drawFlavour(card)
        # Effect #
        effect = self.parseEffect(card)
        effect, _ = self.formatEffect(card, effect, offset = offset)
        self.drawEffect(card, effect)

    def drawQuest(self, card):
        # Frame #
        if card.variant not in ["None", "No Icon", "Extended Art"] and exists("Template/Custom/Quest/" + card.variant):
            card.image.alpha_composite(Image.open("Template/Custom/Quest/" + card.variant).resize((690, 990), 1), (30, 30))
        elif card.variant == "No Icon":
            card.image.alpha_composite(self.quest_frame_no_icon, (30, 30))
        else:
            card.image.alpha_composite(self.quest_frame, (30, 30))
        # Name #
        self.drawName(card, 38, 155, 550, 36)
        # Types and tags #
        self.drawTypeTags(card)
        # Faction #
        self.drawFaction(card)
        # Classes #
        self.drawClasses(card)
        # Background #
        self.drawBackground(card)
        # Flavour #
        offset = 0
        if self.formatFlavour(card):
            offset = self.drawFlavour(card)
        # Effect #
        effect = self.parseEffect(card)
        effect, _ = self.formatEffect(card, effect, offset = offset)
        self.drawEffect(card, effect)

    def drawExtendedArt(self, card):
        image = card.image
        draw = card.draw
        card_type = card.card_type

        x_max_offset = 0
        y_offset = 0

        if card_type == "Ability":
            image.alpha_composite(self.ea_ability_frame, (30, 30))
            self.drawCost(card, 55, 78, 605)

            # Name #
            name_font = ImageFont.truetype(self.main_font_path, 30)
            x_offset = draw.textsize(card.name, font=name_font)[0] / 2
            draw.text((375 - x_offset, 615 - y_offset), card.name, font=name_font, fill=(255, 255, 255, 255), stroke_width=4, stroke_fill=(0, 0, 0, 255))
            # Classes #
            self.drawClasses(card, height=600)
            # Types and tags #
            self.drawTypeTags(card, 25, 160, 682 - x_max_offset, 670 - y_offset)
            # Flavour #
            if self.formatFlavour(card):
                self.drawFlavour(card)
            # Effect #
            effect = self.parseEffect(card)
            effect, _ = self.formatEffect(card, effect, max_height=220)
            self.drawEffect(card, effect, height=725)

        elif card_type == "Ally":
            if card.faction == "Alliance":
                image.alpha_composite(self.ea_alliance_ally_frame, (30, 30))
                x_max_offset = 55
                self.drawCost(card, 55, 103, 740)
            elif card.faction == "Horde":
                image.alpha_composite(self.ea_horde_ally_frame, (30, 30))
                x_max_offset = 55
                self.drawCost(card, 55, 83, 740)
            elif card.faction == "Monster":
                image.alpha_composite(self.ea_monster_ally_frame, (30, 30))
                self.drawCost(card, 55, 83, 744)
            elif card.faction == "Neutral" or card.faction == "Both":
                image.alpha_composite(self.ea_neutral_ally_frame, (30, 30))
                self.drawCost(card, 55, 84, 746)
            elif card.faction == "Scourge":
                image.alpha_composite(self.ea_scourge_ally_frame, (30, 30))
                x_max_offset = 55
                self.drawCost(card, 55, 83, 744)
            self.drawClasses(card, height=750)

            # Types and tags #
            self.drawTypeTags(card, 25, 160, 682 - x_max_offset, 818 - y_offset)
            # Damage #
            if card.damage_type and card.damage:
                damage_type = card.damage_type

                if damage_type == "Arcane":
                    image.alpha_composite(self.ea_arcane_icon, (16, 920))
                elif damage_type == "Chaos":
                    image.alpha_composite(self.ea_chaos_icon, (12, 915))
                elif damage_type == "Fire":
                    image.alpha_composite(self.ea_fire_icon, (27, 914))
                elif damage_type == "Frost":
                    image.alpha_composite(self.ea_frost_icon, (27, 928))
                elif damage_type == "Holy":
                    image.alpha_composite(self.ea_holy_icon, (29, 915))
                elif damage_type == "Melee":
                    image.alpha_composite(self.ea_melee_icon, (28, 917))
                elif damage_type == "Nature":
                    image.alpha_composite(self.ea_nature_icon, (29, 913))
                elif damage_type == "Ranged":
                    image.alpha_composite(self.ea_ranged_icon, (28, 921))
                elif damage_type == "Shadow":
                    image.alpha_composite(self.ea_shadow_icon, (28, 917))

                damage_font = ImageFont.truetype(self.stat_font_path, 55)
                x_offset = draw.textsize(card.damage, font=damage_font)[0] / 2
                draw.text((67 - x_offset, 942), card.damage, font=damage_font, fill=(255, 255, 255, 255), stroke_width=4, stroke_fill=(0, 0, 0, 255))

            # Name #
            name_font = ImageFont.truetype(self.main_font_path, 30)
            x_offset = draw.textsize(card.name, font=name_font)[0] / 2
            draw.text((375 - x_offset, 764 - y_offset), card.name, font=name_font, fill=(255, 255, 255, 255), stroke_width=4, stroke_fill=(0, 0, 0, 255))

            # Health #
            if card_type != "Ability":
                self.drawHealth(card, 55, 680, 942)

            # Flavour #
            if self.formatFlavour(card):
                self.drawFlavour(card)

            effect = self.parseEffect(card)
            effect, _ = self.formatEffect(card, effect, max_height=80)
            self.drawEffect(card, effect, height=875)

        else:
            if card_type == "Equipment":
                class_icons = card.class_icons
                image.alpha_composite(self.ea_equipment_frame, (30, 30))
                y_offset = 148

                self.drawCost(card, 40, 84, 618)

                if card.strike_cost:
                    strike_font = ImageFont.truetype(self.stat_font_path, 55)
                    image.alpha_composite(self.ea_strike_icon, (632, 933))
                    x_offset = draw.textsize(card.strike_cost, font=strike_font)[0] / 2
                    draw.text((677 - x_offset, 940), card.strike_cost, font=strike_font, fill=(255, 255, 255, 255), stroke_width=4, stroke_fill=(0, 0, 0, 255))

                elif card.armor:
                    armor_font = ImageFont.truetype(self.stat_font_path, 55)
                    image.alpha_composite(self.ea_armor_icon, (633, 932))
                    x_offset = draw.textsize(card.armor, font=armor_font)[0] / 2
                    draw.text((677 - x_offset, 940), card.armor, font=armor_font, fill=(255, 255, 255, 255), stroke_width=4, stroke_fill=(0, 0, 0, 255))

                # Class Icons #
                if "Death Knight" in class_icons or class_icons == ["All"]:
                    image.alpha_composite(self.equipment_class_death_knight, (646, 35))
                if "Demon Hunter" in class_icons or class_icons == ["All"]:
                    image.alpha_composite(self.equipment_class_demon_hunter, (646, 87))
                if "Druid" in class_icons or class_icons == ["All"]:
                    image.alpha_composite(self.equipment_class_druid, (646, 139))
                if "Hunter" in class_icons or class_icons == ["All"]:
                    image.alpha_composite(self.equipment_class_hunter, (646, 191))
                if "Mage" in class_icons or class_icons == ["All"]:
                    image.alpha_composite(self.equipment_class_mage, (646, 244))
                if "Monk" in class_icons or class_icons == ["All"]:
                    image.alpha_composite(self.equipment_class_monk_alt, (646, 294))
                if "Paladin" in class_icons or class_icons == ["All"]:
                    image.alpha_composite(self.equipment_class_paladin, (646, 346))
                if "Priest" in class_icons or class_icons == ["All"]:
                    image.alpha_composite(self.equipment_class_priest, (646, 397))
                if "Rogue" in class_icons or class_icons == ["All"]:
                    image.alpha_composite(self.equipment_class_rogue, (646, 448))
                if "Shaman" in class_icons or class_icons == ["All"]:
                    image.alpha_composite(self.equipment_class_shaman, (646, 500))
                if "Warlock" in class_icons or class_icons == ["All"]:
                    image.alpha_composite(self.equipment_class_warlock, (646, 552))
                if "Warrior" in class_icons or class_icons == ["All"]:
                    image.alpha_composite(self.equipment_class_warrior, (646, 604))

            # Types and tags #
            self.drawTypeTags(card, 25, 160, 682 - x_max_offset, 818 - y_offset)
            # Damage #
            if card.damage_type and card.damage:
                damage_type = card.damage_type

                if damage_type == "Arcane":
                    image.alpha_composite(self.ea_arcane_icon, (16, 920))
                elif damage_type == "Chaos":
                    image.alpha_composite(self.ea_chaos_icon, (12, 915))
                elif damage_type == "Fire":
                    image.alpha_composite(self.ea_fire_icon, (27, 914))
                elif damage_type == "Frost":
                    image.alpha_composite(self.ea_frost_icon, (27, 928))
                elif damage_type == "Holy":
                    image.alpha_composite(self.ea_holy_icon, (29, 915))
                elif damage_type == "Melee":
                    image.alpha_composite(self.ea_melee_icon, (28, 917))
                elif damage_type == "Nature":
                    image.alpha_composite(self.ea_nature_icon, (29, 913))
                elif damage_type == "Ranged":
                    image.alpha_composite(self.ea_ranged_icon, (28, 921))
                elif damage_type == "Shadow":
                    image.alpha_composite(self.ea_shadow_icon, (28, 917))

                damage_font = ImageFont.truetype(self.stat_font_path, 55)
                x_offset = draw.textsize(card.damage, font=damage_font)[0] / 2
                draw.text((67 - x_offset, 942), card.damage, font=damage_font, fill=(255, 255, 255, 255), stroke_width=4, stroke_fill=(0, 0, 0, 255))

            # Name #
            name_font = ImageFont.truetype(self.main_font_path, 30)
            x_offset = draw.textsize(card.name, font=name_font)[0] / 2
            draw.text((375 - x_offset, 764 - y_offset), card.name, font=name_font, fill=(255, 255, 255, 255), stroke_width=4, stroke_fill=(0, 0, 0, 255))
            # Health #
            if card.health:
                self.drawHealth(card, 55, 680, 942)

            # Flavour #
            if self.formatFlavour(card):
                self.drawFlavour(card)
            # Effect #
            effect = self.parseEffect(card)
            effect, _ = self.formatEffect(card, effect, max_height=80)
            self.drawEffect(card, effect)
