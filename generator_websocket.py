import asyncio
import websockets
import json

import base64
from io import BytesIO
from PIL import Image
from generator import WoWCardGenerator, Card

wcg = WoWCardGenerator()

async def echo(websocket, path):
    async for message in websocket:
        card_message = json.loads(message)
        if "Connected" in card_message:
            print("New Connection")

        else:
            print("Card Request")
            card = Card()
            # Workaround to handle incomplete Json messages #
            card_json = card.toJson()
            for key in card_message:
                if key in card_json:
                    card_json[key] = card_message[key]

            card.fromJson(card_json)
            if card_message["image_data"]:
                card.art = Image.open(BytesIO(base64.b64decode(card_message["image_data"])))
            wcg.createCard(card)

            buffered = BytesIO()
            card.image.save(buffered, format="PNG")
            buffered.seek(0)
            img_byte = buffered.getvalue()
            card_image_str = "data:image/png;base64," + base64.b64encode(img_byte).decode()

            await websocket.send(card_image_str)

start_server = websockets.serve(echo, "0.0.0.0", 3000)

asyncio.get_event_loop().run_until_complete(start_server)
asyncio.get_event_loop().run_forever()
