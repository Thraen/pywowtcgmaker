let socket = new WebSocket("ws:/127.0.0.1:3000");

let name_field = document.getElementById("name_text_field");
let type_field = document.getElementById("type_combobox");
let variant_field = document.getElementById("variant_combobox");
let cost_field = document.getElementById("cost_text_field");
let instant_field = document.getElementById("instant_checkbox");
let faction = "Neutral";
let health_field = document.getElementById("health_text_field");
let damage_field = document.getElementById("damage_text_field");
let damage_type = "None"
let armor_field = document.getElementById("armor_text_field");
let strike_field = document.getElementById("strike_cost_text_field");
let capacity_field = document.getElementById("capacity_text_field");
let effect_field = document.getElementById("effect_text_box");
let flavour_field = document.getElementById("flavour_text_box");
let classes = [];
let subtypes_field = document.getElementById("subtypes_text_field");
let tags_field = document.getElementById("tags_text_field");
let artist_field = document.getElementById("artist_text_field");
let copyright_field = document.getElementById("copyright_text_field");
let rarity_field = document.getElementById("rarity_combobox");
let block_number_field = document.getElementById("block_number_text_field");
let set_name_field = document.getElementById("set_name_text_field");
let set_number_field = document.getElementById("set_number_text_field");
let set_max_number_field = document.getElementById("set_max_number_text_field");
let border_color_field = document.getElementById("border_color_combobox");
let locale_field = document.getElementById("locale_combobox");

let import_art = document.getElementById("import_art");

socket.onopen = function(e)
{
  socket.send("{\"Connected\":\"True\"}");
};

socket.onmessage = function(e)
{
    console.warn("New Message");
    document.getElementById("card_image").src=e.data;
};

function onTypeChange()
{
    var card_type = type_field.value;

    switch(value)
    {
        case "Ability":


    }
}

function onFactionChange(new_faction)
{
    faction = new_faction;
}

function onClassChange(new_class)
{
    if(classes.includes(new_class))
    {
        for(var i = 0; i < classes.length; ++i)
        {
            if(classes[i] == new_class)
            {
                classes.splice(i, 1);
                break;
            }
        }
    }
    else
    {
        classes.push(new_class);
    }
}

function onDamageTypeChange(new_damage_type)
{
    damage_type = new_damage_type;

    let elements = document.getElementById("damage_type").children;
    let element;

    for(var i = 0; i < elements.length; ++i)
    {
        element = elements[i]
        if(element.value != new_damage_type)
        {
            element.checked = false;
        }
        else
        {
            if(!element.checked)
            {
                damage_type = "None";
            }
        }
    }
}

function onArtButtonClick()
{
    import_art.click();
}

function importArt()
{
}

function generateCard()
{
    var card = {};
    card.name = name_field.value;
    card.card_type = type_field.value;
    card.variant = variant_field.value;
    card.faction = faction;
    card.class_icons = classes;
    card.cost = cost_field.value;
    card.damage = damage_field.value;
    card.damage_type = damage_type;
    card.health = health_field.value;
    card.armor = armor_field.value;
    card.strike_cost = strike_field.value;
    card.capacity = capacity_field.value;
    card.instant = instant_field.checked;
    card.subtypes = subtypes_field.value;
    card.tags = tags_field.value;
    card.professions = [];
    card.effect = effect_field.value;
    card.art_box = [];
    card.flavour = flavour_field.value;
    card.artist = artist_field.value;
    card.copyright = copyright_field.value;
    card.rarity = rarity_field.value;
    card.block_number = block_number_field.value;
    card.set_name = set_name_field.value;
    card.set_number = set_number_field.value;
    card.set_max_number = set_max_number_field.value;
    card.border_color = border_color_field.value;
    card.bleeding = "";
    card.locale_name = locale_field.value;
    card.image_data = false;

    if(import_art.value)
    {
        let file = import_art.files[0];
        if(file.size < (5 * 1024 * 1024))
        {
            var reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = function ()
            {
                card.image_data = reader.result.split(',')[1];
                socket.send(JSON.stringify(card));
            };
            reader.onerror = function (error)
            {
                console.log('Error: ', error);
            };
        }
    }
}
