#!/usr/bin/python3
# -*- coding: utf-8 -*-

from PySide2.QtWidgets import QApplication, QLineEdit, QPushButton, QCheckBox, QGridLayout, QWidget, QHBoxLayout, QVBoxLayout, QTextEdit, QLabel, QComboBox, QScrollArea, QFileDialog, QColorDialog, QMessageBox, QRubberBand
from PySide2.QtGui import QPixmap, QColor, QFont
import PySide2.QtCore as QtCore
from PIL.ImageQt import ImageQt, QImage
from PIL import Image
from math import sqrt

import json
import os
import shutil

from generator import WoWCardGenerator, Card

def dist(xa, ya, xb, yb):
    return sqrt((xb - xa) ** 2 + (yb - ya) ** 2)

class ImageButton(QLabel):
    toggle_signal = QtCore.Signal(str)

    def __init__(self, path, name, linked = False):
        super(ImageButton, self).__init__()
        self.name = name
        self.toggled = False
        self.linked = linked

        # Load pixmaps #
        pixmap = QPixmap()

        pixmap.load(path + name + " Inactive.png")
        self.inactive = pixmap.scaled(50, 50, QtCore.Qt.KeepAspectRatio)

        pixmap.load(path + name + " Hover.png")
        self.hover = pixmap.scaled(50, 50, QtCore.Qt.KeepAspectRatio)

        pixmap.load(path + name + ".png")
        self.active = pixmap.scaled(50, 50, QtCore.Qt.KeepAspectRatio)

        self.setPixmap(self.inactive)

    def activate(self):
        self.toggled = True
        self.setPixmap(self.active)

    def deactivate(self):
        self.toggled = False
        self.setPixmap(self.inactive)

    def mousePressEvent(self, _):
        if self.toggled and not self.linked:
            self.deactivate()
        else:
            self.activate()

        self.toggle_signal.emit(self.name)

    def enterEvent(self, _):
        if not (self.linked and self.toggled):
            self.setPixmap(self.hover)

    def leaveEvent(self, _):
        if self.toggled:
            self.setPixmap(self.active)
        else:
            self.setPixmap(self.inactive)

class CardArtHandler(QLabel):
    def __init__(self, wcg_gui):
        super(CardArtHandler, self).__init__()
        self.wcg_gui = wcg_gui
        self.rubberband = QRubberBand(QRubberBand.Rectangle, self)
        self.ax = 0
        self.ay = 0
        self.bx = 0
        self.by = 0
        self.max_x = 0
        self.max_y = 0
        self.dims = (0, 0)
        self.x_ratio = 0
        self.y_ratio = 0
        self.base_height = 0
        self.rubberband.show()

        self.state = "Idle"

        self.last_pos_x = None
        self.last_pos_y = None

    def mouseMoveEvent(self, event):
        if event.buttons() not in [QtCore.Qt.LeftButton, QtCore.Qt.RightButton]:
            return

        x = int(event.pos().x())
        y = int(event.pos().y())

        if event.buttons() == QtCore.Qt.LeftButton:
            if self.state == "Resize Top Left":
                if self.x_ratio > self.y_ratio:
                    self.ax = min(self.bx - 100, max(0, x))
                    self.ay = int(self.by - (self.bx - self.ax) * self.dims[1] / self.dims[0])
                    if self.ay < 0:
                        self.by -= self.ay
                        self.ay = 0
                else:
                    self.ay = min(self.by - 100, max(0, y))
                    self.ax = int(self.bx - (self.by - self.ay) * self.dims[0] / self.dims[1])
                    if self.ax < 0:
                        self.bx -= self.ax
                        self.ax = 0

            elif self.state == "Resize Bottom Left":
                if self.x_ratio > self.y_ratio:
                    self.ax = min(self.bx - 100, max(0, x))
                    self.by = int(self.ay + (self.bx - self.ax) * self.dims[1] / self.dims[0])
                    if self.by >= self.max_y:
                        self.ay -= self.by - self.max_y + 1
                        self.by = self.max_y - 1
                else:
                    self.by = max(self.ay + 100, min(self.max_y - 1, y))
                    self.ax = int(self.bx - (self.by - self.ay) * self.dims[0] / self.dims[1])
                    if self.ax < 0:
                        self.bx -= self.ax
                        self.ax = 0

            elif self.state == "Resize Top Right":
                if self.x_ratio > self.y_ratio:
                    self.bx = max(self.ax + 100, min(self.max_x - 1, x))
                    self.ay = int(self.by - (self.bx - self.ax) * self.dims[1] / self.dims[0])
                    if self.ay < 0:
                        self.by -= self.ay
                        self.ay = 0
                else:
                    self.ay = min(self.by - 100, max(0, y))
                    self.bx = int(self.ax + (self.by - self.ay) * self.dims[0] / self.dims[1])
                    if self.bx >= self.max_x:
                        self.ax -= self.bx - self.max_x + 1
                        self.bx = self.max_x - 1

            elif self.state == "Resize Bottom Right":
                if self.x_ratio > self.y_ratio:
                    self.bx = max(self.ax + 100, min(self.max_x - 1, x))
                    self.by = int(self.ay + (self.bx - self.ax) * self.dims[1] / self.dims[0])
                    if self.by >= self.max_y:
                        self.ay -= self.by - self.max_y + 1
                        self.by = self.max_y - 1
                else:
                    self.by = max(self.ay + 100, min(self.max_y - 1, y))
                    self.bx = int(self.ax + (self.by - self.ay) * self.dims[0] / self.dims[1])
                    if self.bx >= self.max_x:
                        self.ax -= self.bx - self.max_x + 1
                        self.bx = self.max_x - 1

        # Moving #
        else:
            x_diff = x - self.last_pos_x
            y_diff = y - self.last_pos_y

            if x_diff < 0 and self.ax > 0:
                x_diff = -min(self.ax, -x_diff)
            elif x_diff > 0 and self.bx < self.max_x - 1:
                x_diff = min(self.max_x - 1 - self.bx, x_diff)
            else:
                x_diff = 0
           
            if y_diff < 0 and self.ay > 0:
                y_diff = -min(self.ay, -y_diff)
            elif y_diff > 0 and self.by < self.max_y - 1:
                y_diff = min(self.max_y - 1 - self.by, y_diff)
            else:
                y_diff = 0

            self.ax += x_diff
            self.bx += x_diff
            self.ay += y_diff
            self.by += y_diff

            self.last_pos_x = x
            self.last_pos_y = y

        # Extra sanitize step #
        self.ax = max(0, self.ax)
        self.bx = min(self.max_x - 1, self.bx)
        self.ay = max(0, self.ay)
        self.by = min(self.max_y - 1, self.by)

        self.computeRubberband(True)
        
    def mousePressEvent(self, event):
        x = int(event.pos().x())
        y = int(event.pos().y())

        # Resizing #
        if event.buttons() == QtCore.Qt.LeftButton:    
            if abs(self.ax - x) < abs(x - self.bx):
                if abs(self.ay - y) < abs(y - self.by):
                    self.state = "Resize Top Left"
                else:
                    self.state = "Resize Bottom Left"
            else:
                if abs(self.ay - y) < abs(y - self.by):
                    self.state = "Resize Top Right"
                else:
                    self.state = "Resize Bottom Right"
        
        # Moving #
        if event.buttons() == QtCore.Qt.RightButton:
            self.last_pos_x = x
            self.last_pos_y = y

    def mouseReleaseEvent(self, event):
        ratio = self.base_height / self.max_y
        self.wcg_gui.card.art_box = (int(self.ax * ratio), int(self.ay * ratio), int(self.bx * ratio), int(self.by * ratio))
        self.wcg_gui.generateCard()
        self.state = "Idle"
        self.last_dist = 0

    def setArt(self, art, base_height, update=True):
        self.setPixmap(art)
        self.base_height = base_height
        self.max_x = art.rect().bottomRight().x()
        self.max_y = art.rect().bottomRight().y()

        if update:
            self.computeRubberband(new_art=True)
            self.mouseReleaseEvent(None)

    def computeRubberband(self, mouse_update=False, new_art=False):
        if self.wcg_gui.card.variant == "Extended Art" or "Back" in self.wcg_gui.card.variant:
            if self.dims != (690, 990):
                self.dims = (690, 990)
            elif not mouse_update and not new_art:
                return
        else:
            if self.dims != (690, 565):
                self.dims = (690, 565)
            elif not mouse_update and not new_art:
                return

        if not mouse_update:
            ratio = self.base_height / self.max_y
            # Get longer side taking card type into account #
            self.x_ratio =  self.dims[0] / self.max_x
            self.y_ratio = self.dims[1] / self.max_y
            if self.x_ratio > self.y_ratio:
                self.ax = 0
                self.bx = self.max_x
                height = int(self.max_x * self.dims[1] / self.dims[0])
                self.ay = int((self.max_y - height) / 2)
                self.by = self.ay + height
                self.rubberband.setGeometry(QtCore.QRect(self.ax, self.ay, self.max_x, height))

            else:
                self.ay = 0
                self.by = self.max_y
                width = int(self.max_y * self.dims[0] / self.dims[1])
                self.ax = int((self.max_x - width) / 2)
                self.bx = self.ax + width
                self.rubberband.setGeometry(QtCore.QRect(self.ax, self.ay, width, self.max_y))
                
            self.wcg_gui.card.art_box = (int(self.ax * ratio), int(self.ay * ratio), int(self.bx * ratio), int(self.by * ratio))

        else:
            self.rubberband.setGeometry(QtCore.QRect(self.ax, self.ay, self.bx - self.ax, self.by - self.ay))

class CardPreview(QLabel):

    def __init__(self, card):
        super(CardPreview, self).__init__()
        self.card = card
        self.preview_image = None
        self.selected_preview_image = None

    def mousePressEvent(self, _):
        self.toggle_signal.emit(self)

CardPreview.toggle_signal = QtCore.Signal(CardPreview)


class WoWCardGeneratorGUI():

    def __init__(self):
        # Instanciate the card generator #
        self.wcg = WoWCardGenerator()
        self.lock_generation = False
        self.card = Card()
        self.config = {}
        self.bleeding = [750, 1050]

        if os.path.exists("config.json"):
            config_file = open("config.json", encoding="utf-8")
            self.config = json.load(config_file)
            config_file.close()

        if not "default_directory" in self.config:
            self.config["default_directory"] = ""

        # Create custom folder hierarchy #
        if not os.path.exists("Template/Custom"):
            os.mkdir("Template/Custom")
        if not os.path.exists("Template/Custom/Ability"):
            os.mkdir("Template/Custom/Ability")
        if not os.path.exists("Template/Custom/Ally"):
            os.mkdir("Template/Custom/Ally")
        if not os.path.exists("Template/Custom/Background"):
            os.mkdir("Template/Custom/Background")
        if not os.path.exists("Template/Custom/Equipment"):
            os.mkdir("Template/Custom/Equipment")
        if not os.path.exists("Template/Custom/Hero"):
            os.mkdir("Template/Custom/Hero")
        if not os.path.exists("Template/Custom/Master Hero"):
            os.mkdir("Template/Custom/Master Hero")
        if not os.path.exists("Template/Custom/Location"):
            os.mkdir("Template/Custom/Location")
        if not os.path.exists("Template/Custom/Quest"):
            os.mkdir("Template/Custom/Quest")

        # Create the application #
        self.app = QApplication([])

        if not "font_size" in self.config:
            self.font_size = self.app.font().pointSize()

        else:
            self.font_size = self.config["font_size"]
            font = self.app.font()
            font.setPointSize(self.font_size)
            self.app.setFont(font)

        # Create the main window #
        self.main_frame = QWidget()
        self.main_frame.setWindowTitle("WoWCardGenerator")

        # Load default locale #
        locale_file = open("Locales/English.json", "r", encoding="utf-8")
        self.locale = json.load(locale_file)
        locale_file.close()

        # Instantiate the layouts #
        self.main_layout = QHBoxLayout()
        self.left_panel = QGridLayout()
        self.left_panel.setAlignment(QtCore.Qt.AlignTop)
        self.middle_panel = QGridLayout()
        self.middle_panel.setAlignment(QtCore.Qt.AlignTop)
        self.right_panel = QGridLayout()
        self.right_panel.setAlignment(QtCore.Qt.AlignTop)
        self.preview_panel = QVBoxLayout()
        self.preview_panel.setAlignment(QtCore.Qt.AlignTop)
        self.main_layout.addLayout(self.left_panel)
        self.main_layout.addLayout(self.middle_panel)
        self.main_layout.addLayout(self.right_panel)
        self.main_layout.addLayout(self.preview_panel)
        self.main_frame.setLayout(self.main_layout)

        self.left_rows = [0, 0]
        self.middle_rows = [0, 0]
        self.right_rows = [0]

        ## Left panel ##

        # Name #
        self.name_label = QLabel(self.locale["name"])
        self.name_text = QLineEdit()
        self.name_text.textChanged.connect(self.nameChanged)

        # Type #
        self.type_label = QLabel(self.locale["type"])
        self.type_combo_box = QComboBox()
        self.type_combo_box.addItems(self.locale["type_names"])
        self.type_combo_box.currentIndexChanged.connect(self.cardTypeChanged)

        self.type_names = self.locale["type_names"]

        # Type override #
        self.type_override_label = QLabel(self.locale["type_override"])
        self.type_override_text = QLineEdit()
        self.type_override_text.textChanged.connect(self.typeOverrideChanged)

        # Variant #
        self.variant_label = QLabel(self.locale["variant"])
        self.variant_combo_box = QComboBox()
        self.variant_combo_box.currentIndexChanged.connect(self.variantChanged)
        self.variant_combo_box.setSizeAdjustPolicy(QComboBox.AdjustToContents)

        self.variant_names = self.locale["variant_names"]

        # Faction #
        self.faction_label = QLabel(self.locale["faction"])
        self.faction_icons = QWidget()
        faction_icons_layout = QHBoxLayout()
        self.faction_icons.setLayout(faction_icons_layout)

        # Buttons dict #
        self.faction_button_dict = {}

        # Row #
        alliance_icon_button = ImageButton("Template/Editor/Factions/", "Alliance", True)
        alliance_icon_button.toggle_signal.connect(self.factionToggled)
        self.faction_button_dict["Alliance"] = alliance_icon_button
        faction_icons_layout.addWidget(alliance_icon_button)

        both_icon_button = ImageButton("Template/Editor/Factions/", "Both", True)
        both_icon_button.toggle_signal.connect(self.factionToggled)
        self.faction_button_dict["Both"] = both_icon_button
        faction_icons_layout.addWidget(both_icon_button)

        horde_icon_button = ImageButton("Template/Editor/Factions/", "Horde", True)
        horde_icon_button.toggle_signal.connect(self.factionToggled)
        self.faction_button_dict["Horde"] = horde_icon_button
        faction_icons_layout.addWidget(horde_icon_button)

        monster_icon_button = ImageButton("Template/Editor/Factions/", "Monster", True)
        monster_icon_button.toggle_signal.connect(self.factionToggled)
        self.faction_button_dict["Monster"] = monster_icon_button
        faction_icons_layout.addWidget(monster_icon_button)

        neutral_icon_button = ImageButton("Template/Editor/Factions/", "Neutral", True)
        neutral_icon_button.toggle_signal.connect(self.factionToggled)
        self.faction_button_dict["Neutral"] = neutral_icon_button
        faction_icons_layout.addWidget(neutral_icon_button)

        scourge_icon_button = ImageButton("Template/Editor/Factions/", "Scourge", True)
        scourge_icon_button.toggle_signal.connect(self.factionToggled)
        self.faction_button_dict["Scourge"] = scourge_icon_button
        faction_icons_layout.addWidget(scourge_icon_button)

        self.current_faction_button = neutral_icon_button

        # Cost #
        self.cost_label = QLabel(self.locale["cost"])
        self.cost_text = QLineEdit()
        self.cost_text.textChanged.connect(self.costChanged)

        # Instant #
        self.instant_label = QLabel(self.locale["instant"])
        self.instant_checkbox = QCheckBox()
        self.instant_checkbox.clicked.connect(self.instantChanged)

        # Health #
        self.health_label = QLabel(self.locale["health"])
        self.health_text = QLineEdit()
        self.health_text.textChanged.connect(self.healthChanged)

        # Damage #
        self.damage_label = QLabel(self.locale["damage"])
        self.damage_text = QLineEdit()
        self.damage_text.textChanged.connect(self.damageChanged)

        # Armor #
        self.armor_label = QLabel(self.locale["armor"])
        self.armor_text = QLineEdit()
        self.armor_text.textChanged.connect(self.armorChanged)

        # Strike cost #
        self.strike_cost_label = QLabel(self.locale["strike_cost"])
        self.strike_cost_text = QLineEdit()
        self.strike_cost_text.textChanged.connect(self.strikeCostChanged)

        # Capacity #
        self.capacity_label = QLabel(self.locale["capacity"])
        self.capacity_text = QLineEdit()
        self.capacity_text.textChanged.connect(self.capacityChanged)

        # Effect #
        self.effect_label = QLabel(self.locale["effect"])
        self.effect_text = QTextEdit()
        self.effect_text.textChanged.connect(self.effectChanged)

        # Flavour #
        self.flavour_label = QLabel(self.locale["flavour"])
        self.flavour_text = QTextEdit()
        self.flavour_text.textChanged.connect(self.flavourChanged)

        # Background #
        self.background_label = QLabel(self.locale["background"])
        self.background_combo_box = QComboBox()
        self.background_combo_box.addItem("None")
        self.background_combo_box.addItems(os.listdir("Template/Custom/Background"))
        self.background_combo_box.currentIndexChanged.connect(self.backgroundChanged)

        ## Middle panel ##

        # Class icons #

        # Layouts #
        self.class_icons = QWidget()
        class_icons_layout = QVBoxLayout()
        self.class_icons.setLayout(class_icons_layout)
        class_row0_layout = QHBoxLayout()
        class_row1_layout = QHBoxLayout()
        class_row2_layout = QHBoxLayout()
        class_row3_layout = QHBoxLayout()

        # Class Dict #
        self.class_button_dict = {}

        # Label #
        self.class_label = QLabel(self.locale["classes"])
        class_icons_layout.addWidget(self.class_label)
        self.class_label.setAlignment(QtCore.Qt.AlignHCenter)

        # Row 0 #
        dk_icon_button = ImageButton("Template/Editor/Class Icons/", "Death Knight")
        dk_icon_button.toggle_signal.connect(self.classToggled)
        self.class_button_dict["Death Knight"] = dk_icon_button
        class_row0_layout.addWidget(dk_icon_button)

        dh_icon_button = ImageButton("Template/Editor/Class Icons/", "Demon Hunter")
        dh_icon_button.toggle_signal.connect(self.classToggled)
        self.class_button_dict["Demon Hunter"] = dh_icon_button
        class_row0_layout.addWidget(dh_icon_button)

        dr_icon_button = ImageButton("Template/Editor/Class Icons/", "Druid")
        dr_icon_button.toggle_signal.connect(self.classToggled)
        self.class_button_dict["Druid"] = dr_icon_button
        class_row0_layout.addWidget(dr_icon_button)

        class_icons_layout.addLayout(class_row0_layout)

        # Row 1 #
        hu_icon_button = ImageButton("Template/Editor/Class Icons/", "Hunter")
        hu_icon_button.toggle_signal.connect(self.classToggled)
        self.class_button_dict["Hunter"] = hu_icon_button
        class_row1_layout.addWidget(hu_icon_button)

        ma_icon_button = ImageButton("Template/Editor/Class Icons/", "Mage")
        ma_icon_button.toggle_signal.connect(self.classToggled)
        self.class_button_dict["Mage"] = ma_icon_button
        class_row1_layout.addWidget(ma_icon_button)

        mo_icon_button = ImageButton("Template/Editor/Class Icons/", "Monk")
        mo_icon_button.toggle_signal.connect(self.classToggled)
        self.class_button_dict["Monk"] = mo_icon_button
        class_row1_layout.addWidget(mo_icon_button)

        class_icons_layout.addLayout(class_row1_layout)

        # Row 2 #
        pa_icon_button = ImageButton("Template/Editor/Class Icons/", "Paladin")
        pa_icon_button.toggle_signal.connect(self.classToggled)
        self.class_button_dict["Paladin"] = pa_icon_button
        class_row2_layout.addWidget(pa_icon_button)

        pr_icon_button = ImageButton("Template/Editor/Class Icons/", "Priest")
        pr_icon_button.toggle_signal.connect(self.classToggled)
        self.class_button_dict["Priest"] = pr_icon_button
        class_row2_layout.addWidget(pr_icon_button)

        ro_icon_button = ImageButton("Template/Editor/Class Icons/", "Rogue")
        ro_icon_button.toggle_signal.connect(self.classToggled)
        self.class_button_dict["Rogue"] = ro_icon_button
        class_row2_layout.addWidget(ro_icon_button)

        class_icons_layout.addLayout(class_row2_layout)

        # Row 3 #
        sa_icon_button = ImageButton("Template/Editor/Class Icons/", "Shaman")
        sa_icon_button.toggle_signal.connect(self.classToggled)
        self.class_button_dict["Shaman"] = sa_icon_button
        class_row3_layout.addWidget(sa_icon_button)

        wk_icon_button = ImageButton("Template/Editor/Class Icons/", "Warlock")
        wk_icon_button.toggle_signal.connect(self.classToggled)
        self.class_button_dict["Warlock"] = wk_icon_button
        class_row3_layout.addWidget(wk_icon_button)

        wr_icon_button = ImageButton("Template/Editor/Class Icons/", "Warrior")
        wr_icon_button.toggle_signal.connect(self.classToggled)
        self.class_button_dict["Warrior"] = wr_icon_button
        class_row3_layout.addWidget(wr_icon_button)

        class_icons_layout.addLayout(class_row3_layout)

        # Damage icons #

        # Layouts #
        self.damage_icons = QWidget()
        damage_icons_layout = QVBoxLayout()
        self.damage_icons.setLayout(damage_icons_layout)
        damage_row0_layout = QHBoxLayout()
        damage_row1_layout = QHBoxLayout()
        damage_row2_layout = QHBoxLayout()

        # Label #
        self.damage_type_label = QLabel(self.locale["damage_type"])
        damage_icons_layout.addWidget(self.damage_type_label)
        self.damage_type_label.setAlignment(QtCore.Qt.AlignHCenter)

        # Buttons dict and current #
        self.damage_button_dict = {}
        self.current_damage_button = None

        # Row 0 #
        ar_icon_button = ImageButton("Template/Editor/Damage Icons/", "Arcane")
        ar_icon_button.toggle_signal.connect(self.damageToggled)
        damage_row0_layout.addWidget(ar_icon_button)
        self.damage_button_dict["Arcane"] = ar_icon_button

        ch_icon_button = ImageButton("Template/Editor/Damage Icons/", "Chaos")
        ch_icon_button.toggle_signal.connect(self.damageToggled)
        damage_row0_layout.addWidget(ch_icon_button)
        self.damage_button_dict["Chaos"] = ch_icon_button

        fi_icon_button = ImageButton("Template/Editor/Damage Icons/", "Fire")
        fi_icon_button.toggle_signal.connect(self.damageToggled)
        damage_row0_layout.addWidget(fi_icon_button)
        self.damage_button_dict["Fire"] = fi_icon_button

        damage_icons_layout.addLayout(damage_row0_layout)

        # Row 1 #
        fo_icon_button = ImageButton("Template/Editor/Damage Icons/", "Frost")
        fo_icon_button.toggle_signal.connect(self.damageToggled)
        damage_row1_layout.addWidget(fo_icon_button)
        self.damage_button_dict["Frost"] = fo_icon_button

        ho_icon_button = ImageButton("Template/Editor/Damage Icons/", "Holy")
        ho_icon_button.toggle_signal.connect(self.damageToggled)
        damage_row1_layout.addWidget(ho_icon_button)
        self.damage_button_dict["Holy"] = ho_icon_button

        me_icon_button = ImageButton("Template/Editor/Damage Icons/", "Melee")
        me_icon_button.toggle_signal.connect(self.damageToggled)
        damage_row1_layout.addWidget(me_icon_button)
        self.damage_button_dict["Melee"] = me_icon_button

        damage_icons_layout.addLayout(damage_row1_layout)

        # Row 2 #
        na_icon_button = ImageButton("Template/Editor/Damage Icons/", "Nature")
        na_icon_button.toggle_signal.connect(self.damageToggled)
        damage_row2_layout.addWidget(na_icon_button)
        self.damage_button_dict["Nature"] = na_icon_button

        ra_icon_button = ImageButton("Template/Editor/Damage Icons/", "Ranged")
        ra_icon_button.toggle_signal.connect(self.damageToggled)
        damage_row2_layout.addWidget(ra_icon_button)
        self.damage_button_dict["Ranged"] = ra_icon_button

        sh_icon_button = ImageButton("Template/Editor/Damage Icons/", "Shadow")
        sh_icon_button.toggle_signal.connect(self.damageToggled)
        damage_row2_layout.addWidget(sh_icon_button)
        self.damage_button_dict["Shadow"] = sh_icon_button

        damage_icons_layout.addLayout(damage_row2_layout)

        # Subtypes #
        self.subtypes_label = QLabel(self.locale["subtypes"])
        self.subtypes_text = QLineEdit()
        self.subtypes_text.textChanged.connect(self.subtypesChanged)

        # Tags #
        self.tags_label = QLabel(self.locale["tags"])
        self.tags_text = QLineEdit()
        self.tags_text.textChanged.connect(self.tagsChanged)

        # Rarity #
        self.rarity_label = QLabel(self.locale["rarity"])
        self.rarity_names = self.locale["rarity_names"]

        self.rarity_combo_box = QComboBox()
        self.rarity_combo_box.addItems(self.locale["rarity_names"])
        self.rarity_combo_box.currentIndexChanged.connect(self.rarityChanged)

        # Block number #
        self.block_number_label = QLabel(self.locale["block_number"])
        self.block_number_text = QLineEdit()
        self.block_number_text.textChanged.connect(self.blockNumberChanged)

        # Set name #
        self.set_name_label = QLabel(self.locale["set_name"])
        self.set_name_text = QLineEdit()
        self.set_name_text.textChanged.connect(self.setNameChanged)

        # Set number widget #
        self.set_number_label = QLabel(self.locale["set_number"])
        self.set_number_widget = QWidget()
        set_number_layout = QHBoxLayout()
        self.set_number_widget.setLayout(set_number_layout)

        self.set_number_text = QLineEdit()
        self.set_number_text.textChanged.connect(self.setNumberChanged)
        set_number_layout.addWidget(self.set_number_text)

        set_number_separator = QLabel()
        set_number_separator.setText("/")
        set_number_layout.addWidget(set_number_separator)

        self.set_max_number_text = QLineEdit()
        self.set_max_number_text.textChanged.connect(self.setMaxNumberChanged)
        set_number_layout.addWidget(self.set_max_number_text)

        ## Right panel ##

        # Image buttons layout #
        self.image_buttons = QWidget()
        image_buttons_layout = QHBoxLayout()

        # Import art button #
        self.import_art_button = QPushButton(self.locale["import_art"])
        self.import_art_button.clicked.connect(self.selectArt)
        image_buttons_layout.addWidget(self.import_art_button)

        # Resize art button #
        self.resize_art_button = QPushButton(self.locale["resize_art"])
        self.resize_art_button.clicked.connect(self.resizeArt)
        image_buttons_layout.addWidget(self.resize_art_button)

        # Save card button #
        self.save_card_button = QPushButton(self.locale["save_card"])
        self.save_card_button.clicked.connect(self.saveCard)
        image_buttons_layout.addWidget(self.save_card_button)

        self.image_buttons.setLayout(image_buttons_layout)

        # Save all cards button #
        self.save_all_cards_button = QPushButton(self.locale["save_all_cards"])
        self.save_all_cards_button.clicked.connect(self.saveAllCards)
        image_buttons_layout.addWidget(self.save_all_cards_button)

        self.image_buttons.setLayout(image_buttons_layout)

        # Save to printable button #
        self.save_to_printable_button = QPushButton(self.locale["save_to_printable"])
        self.save_to_printable_button.clicked.connect(self.saveToPrintable)
        image_buttons_layout.addWidget(self.save_to_printable_button)

        # Artist widget #
        self.artist_widget = QWidget()
        artist_layout = QHBoxLayout()
        self.artist_widget.setLayout(artist_layout)

        self.artist_label = QLabel(self.locale["artist"])
        artist_layout.addWidget(self.artist_label)

        self.artist_text = QLineEdit()
        self.artist_text.textChanged.connect(self.artistChanged)
        artist_layout.addWidget(self.artist_text)

        # Copyright widget #
        self.copyright_widget = QWidget()
        copyright_layout = QHBoxLayout()
        self.copyright_widget.setLayout(copyright_layout)

        self.copyright_label = QLabel(self.locale["copyright"])
        copyright_layout.addWidget(self.copyright_label)

        self.copyright_text = QLineEdit()
        self.copyright_text.textChanged.connect(self.copyrightChanged)
        copyright_layout.addWidget(self.copyright_text)

        # Border widget #
        self.border_widget = QWidget()
        border_layout = QHBoxLayout()
        self.border_widget.setLayout(border_layout)

        self.border_label = QLabel(self.locale["border"])
        border_layout.addWidget(self.border_label)

        self.border_color_button = QPushButton()
        self.border_color_button.clicked.connect(self.borderClicked)
        self.border_color_button.setStyleSheet("QPushButton{background-color : #000000;}")
        border_layout.addWidget(self.border_color_button)

        self.border_sharp_label = QLabel(self.locale["sharp"])
        border_layout.addWidget(self.border_sharp_label)

        self.border_sharp_checkbox = QCheckBox()
        self.border_sharp_checkbox.clicked.connect(self.borderSharpChanged)
        border_layout.addWidget(self.border_sharp_checkbox)

        self.bleeding_label = QLabel(self.locale["bleeding"])
        border_layout.addWidget(self.bleeding_label)

        self.bleeding_x_text = QLineEdit("750")
        self.bleeding_x_text.textChanged.connect(self.bleedingXChanged)
        border_layout.addWidget(self.bleeding_x_text)

        self.bleeding_y_text = QLineEdit("1050")
        self.bleeding_y_text.textChanged.connect(self.bleedingYChanged)
        border_layout.addWidget(self.bleeding_y_text)

        # JSON related buttons #
        self.json_load_button = QPushButton(self.locale["load_json"])
        self.json_load_button.clicked.connect(self.loadFromJSON)

        self.json_save_button = QPushButton(self.locale["save_json"])
        self.json_save_button.clicked.connect(self.saveToJSON)

        # Language / Font / Help widget #
        self.language_font_help_widget = QWidget()
        language_font_help_layout = QHBoxLayout()
        self.language_font_help_widget.setLayout(language_font_help_layout)

        self.language_label = QLabel(self.locale["language"])
        language_font_help_layout.addWidget(self.language_label)

        language_list = []
        for language_file in os.listdir("Locales"):
            language_list.append(language_file.split(".")[0])

        self.language_combo_box = QComboBox()
        self.language_combo_box.addItems(language_list)
        self.language_combo_box.setCurrentIndex(language_list.index("English"))
        self.language_combo_box.currentIndexChanged.connect(self.languageChanged)
        language_font_help_layout.addWidget(self.language_combo_box)

        self.font_label = QLabel(self.locale["font"])
        language_font_help_layout.addWidget(self.font_label)

        self.font_minus_button = QPushButton("-")
        self.font_minus_button.clicked.connect(self.fontMinusButtonClicked)
        language_font_help_layout.addWidget(self.font_minus_button)

        self.font_plus_button = QPushButton("+")
        self.font_plus_button.clicked.connect(self.fontPlusButtonClicked)
        language_font_help_layout.addWidget(self.font_plus_button)

        self.help_button = QPushButton("Help")
        self.help_button.clicked.connect(self.helpClicked)
        language_font_help_layout.addWidget(self.help_button)

        self.directory_button = QPushButton("Default directory")
        self.directory_button.clicked.connect(self.defaultDirectoryClicked)
        language_font_help_layout.addWidget(self.directory_button)

        # Non manual switch bool #
        self.nonManualLanguageSwitch = False

        # Card image #
        self.card_image = QLabel()

        ## Preview Panel ##
        self.preview_array = []
        self.preview_layout = QGridLayout()
        self.preview_layout.setAlignment(QtCore.Qt.AlignTop | QtCore.Qt.AlignLeft)
        preview_widget = QWidget()
        preview_widget.setLayout(self.preview_layout)

        self.preview_table = 0
        self.preview_row = 0
        self.preview_column = 1

        # Invisible preview layout holds unused preview (widget becomes a window if no parent and set invisible is slow)
        self.hidden_preview_layout = QHBoxLayout()
        self.hidden_preview_widget = QWidget()
        self.hidden_preview_widget.setLayout(self.hidden_preview_layout)
        self.hidden_preview_widget.setVisible(False)

        # Empty slot image #
        self.empty_slot_image = QPixmap.fromImage(ImageQt(Image.open("Template/Card Back Grayscale.png")).scaledToWidth(100, QtCore.Qt.SmoothTransformation))

        # Selected card border #
        self.selected_border = Image.open("Template/Card Selected.png")

        # Create card button #
        create_card_button = QPushButton(self.locale["create_card"])
        create_card_button.clicked.connect(self.createCardClicked)
        self.preview_panel.addWidget(create_card_button)

        # Previous / next page buttons #
        self.page_buttons_widget = QWidget()
        page_button_layout = QHBoxLayout()
        previous_preview_page_button = QPushButton("<=")
        previous_preview_page_button.clicked.connect(self.previousPreviewTableClicked)
        page_button_layout.addWidget(previous_preview_page_button)
        next_preview_page_button = QPushButton("=>")
        next_preview_page_button.clicked.connect(self.nextPreviewTableClicked)
        page_button_layout.addWidget(next_preview_page_button)
        self.page_buttons_widget.setLayout(page_button_layout)
        self.preview_panel.addWidget(self.page_buttons_widget)

        # Add preview zone #
        self.preview_panel.addWidget(preview_widget)

        # Delete card button #
        delete_card_button = QPushButton(self.locale["delete_card"])
        delete_card_button.clicked.connect(self.deleteCardClicked)
        self.preview_panel.addWidget(delete_card_button)

        # Set first card preview #
        self.card_preview = CardPreview(self.card)
        self.card_preview.toggle_signal.connect(self.previewClicked)
        self.preview_array.append(self.card_preview)
        self.preview_layout.addWidget(self.card_preview, 0, 0)

        # Lock generation #
        self.lockGeneration()

        # Toggle default faction #
        self.faction_button_dict["Neutral"].activate()
        self.factionToggled("Neutral")

        # Create border frame #
        self.border_frame = QWidget()
        self.border_frame.setWindowTitle(self.locale["border"])
        border_frame_layout = QHBoxLayout()
        self.border_frame.setLayout(border_frame_layout)
        self.border_color_dialog = QColorDialog()
        self.border_color_dialog.setOption(QColorDialog.NoButtons)
        border_frame_layout.addWidget(self.border_color_dialog)
        self.border_color_dialog.currentColorChanged.connect(self.borderColorChanged)

        # Create resize frame #
        self.resize_art_frame = QWidget()
        self.resize_art_frame.setWindowTitle(self.locale["resize_art"])
        resize_art_frame_layout = QHBoxLayout()
        self.resize_art_frame.setLayout(resize_art_frame_layout)

        # Card handler init #
        self.card_art_handler = CardArtHandler(self)
        resize_art_frame_layout.addWidget(self.card_art_handler)

        # Create help frame #
        self.help_frame = QWidget()
        self.help_frame.setWindowTitle(self.locale["help"])
        help_frame_layout = QHBoxLayout()
        self.help_frame.setLayout(help_frame_layout)

        left_help_frame_layout = QVBoxLayout()
        help_frame_layout.addLayout(left_help_frame_layout)
        right_help_frame_layout = QVBoxLayout()
        help_frame_layout.addLayout(right_help_frame_layout)

        self.help_text = QLabel(self.locale["help_text"])
        left_help_frame_layout.addWidget(self.help_text)

        # Load base card state #
        self.loadCardState()

        # Unlock generation #
        self.unlockGeneration()

        # Set art handler default art and generate #
        dummy_image = Image.open("Template/Dummy.png")
        image = QPixmap.fromImage(ImageQt(dummy_image).scaledToHeight(500, QtCore.Qt.SmoothTransformation))
        self.card_art_handler.setArt(image, dummy_image.height)
        dummy_image.close()

    def run(self):
        # Draw default GUI #
        self.drawGUI()
        # Show the window #
        self.main_frame.showMaximized()

        # Run the main Qt loop #
        self.app.exec_()

    def clear(self, layout):
        for index in reversed(range(layout.count())):
            widget_to_remove = layout.itemAt(index).widget()
            if isinstance(widget_to_remove, QWidget):
                widget_to_remove.setParent(None)

    def clearGUI(self):
        self.clear(self.left_panel)
        self.clear(self.middle_panel)
        self.clear(self.right_panel)

        self.left_rows = [0, 0]
        self.middle_rows = [0, 0]
        self.right_rows = [0]

    def draw(self, layout, widget, rows_number, column):
        widget_alignment = QtCore.Qt.AlignBaseline
        if column == 0  and layout != self.right_panel:
            widget_alignment = QtCore.Qt.AlignRight
        if widget == self.card_image:
            widget_alignment = QtCore.Qt.AlignHCenter
        layout.addWidget(widget, rows_number[column], column, alignment=widget_alignment)
        rows_number[column] += 1

    def drawGUI(self):
        # Draw default widgets #

        # Name #
        self.draw(self.left_panel, self.name_label, self.left_rows, 0)
        self.draw(self.left_panel, self.name_text, self.left_rows, 1)
        self.draw(self.left_panel, self.type_label, self.left_rows, 0)
        self.draw(self.left_panel, self.type_combo_box, self.left_rows, 1)
        self.draw(self.left_panel, self.type_override_label, self.left_rows, 0)
        self.draw(self.left_panel, self.type_override_text, self.left_rows, 1)

        self.draw(self.right_panel, self.image_buttons, self.right_rows, 0)
        self.draw(self.right_panel, self.card_image, self.right_rows, 0)
        self.draw(self.right_panel, self.artist_widget, self.right_rows, 0)
        self.draw(self.right_panel, self.copyright_widget, self.right_rows, 0)
        self.draw(self.right_panel, self.border_widget, self.right_rows, 0)
        self.draw(self.right_panel, self.json_load_button, self.right_rows, 0)
        self.draw(self.right_panel, self.json_save_button, self.right_rows, 0)
        self.draw(self.right_panel, self.language_font_help_widget, self.right_rows, 0)

        card_type_index = self.type_combo_box.currentIndex()

        # Draw type dependant widgets #
        # Ability #
        if card_type_index == 0:
            self.draw(self.left_panel, self.variant_label, self.left_rows, 0)
            self.draw(self.left_panel, self.variant_combo_box, self.left_rows, 1)
            self.draw(self.left_panel, self.cost_label, self.left_rows, 0)
            self.draw(self.left_panel, self.cost_text, self.left_rows, 1)
            self.draw(self.left_panel, self.instant_label, self.left_rows, 0)
            self.draw(self.left_panel, self.instant_checkbox, self.left_rows, 1)
            self.draw(self.left_panel, self.faction_label, self.left_rows, 0)
            self.draw(self.left_panel, self.faction_icons, self.left_rows, 1)

            self.draw(self.middle_panel, self.class_label, self.middle_rows, 0)
            self.draw(self.middle_panel, self.class_icons, self.middle_rows, 1)

        # Ally #
        elif card_type_index == 1:
            self.draw(self.left_panel, self.variant_label, self.left_rows, 0)
            self.draw(self.left_panel, self.variant_combo_box, self.left_rows, 1)
            self.draw(self.left_panel, self.cost_label, self.left_rows, 0)
            self.draw(self.left_panel, self.cost_text, self.left_rows, 1)
            self.draw(self.left_panel, self.instant_label, self.left_rows, 0)
            self.draw(self.left_panel, self.instant_checkbox, self.left_rows, 1)
            self.draw(self.left_panel, self.faction_label, self.left_rows, 0)
            self.draw(self.left_panel, self.faction_icons, self.left_rows, 1)
            self.draw(self.left_panel, self.health_label, self.left_rows, 0)
            self.draw(self.left_panel, self.health_text, self.left_rows, 1)
            self.draw(self.left_panel, self.damage_label, self.left_rows, 0)
            self.draw(self.left_panel, self.damage_text, self.left_rows, 1)

            self.draw(self.middle_panel, self.class_label, self.middle_rows, 0)
            self.draw(self.middle_panel, self.class_icons, self.middle_rows, 1)
            self.draw(self.middle_panel, self.damage_type_label, self.middle_rows, 0)
            self.draw(self.middle_panel, self.damage_icons, self.middle_rows, 1)

        # Equipment #
        elif card_type_index == 2:
            self.draw(self.left_panel, self.variant_label, self.left_rows, 0)
            self.draw(self.left_panel, self.variant_combo_box, self.left_rows, 1)
            self.draw(self.left_panel, self.cost_label, self.left_rows, 0)
            self.draw(self.left_panel, self.cost_text, self.left_rows, 1)
            self.draw(self.left_panel, self.instant_label, self.left_rows, 0)
            self.draw(self.left_panel, self.instant_checkbox, self.left_rows, 1)
            self.draw(self.left_panel, self.faction_label, self.left_rows, 0)
            self.draw(self.left_panel, self.faction_icons, self.left_rows, 1)
            self.draw(self.left_panel, self.damage_label, self.left_rows, 0)
            self.draw(self.left_panel, self.damage_text, self.left_rows, 1)
            self.draw(self.left_panel, self.armor_label, self.left_rows, 0)
            self.draw(self.left_panel, self.armor_text, self.left_rows, 1)
            self.draw(self.left_panel, self.strike_cost_label, self.left_rows, 0)
            self.draw(self.left_panel, self.strike_cost_text, self.left_rows, 1)
            self.draw(self.left_panel, self.health_label, self.left_rows, 0)
            self.draw(self.left_panel, self.health_text, self.left_rows, 1)

            self.draw(self.middle_panel, self.class_label, self.middle_rows, 0)
            self.draw(self.middle_panel, self.class_icons, self.middle_rows, 1)
            self.draw(self.middle_panel, self.damage_type_label, self.middle_rows, 0)
            self.draw(self.middle_panel, self.damage_icons, self.middle_rows, 1)

        # Hero #
        elif card_type_index == 3:
            self.draw(self.left_panel, self.variant_label, self.left_rows, 0)
            self.draw(self.left_panel, self.variant_combo_box, self.left_rows, 1)
            self.draw(self.left_panel, self.faction_label, self.left_rows, 0)
            self.draw(self.left_panel, self.faction_icons, self.left_rows, 1)
            self.draw(self.left_panel, self.health_label, self.left_rows, 0)
            self.draw(self.left_panel, self.health_text, self.left_rows, 1)
            self.draw(self.left_panel, self.damage_label, self.left_rows, 0)
            self.draw(self.left_panel, self.damage_text, self.left_rows, 1)

            self.draw(self.middle_panel, self.class_label, self.middle_rows, 0)
            self.draw(self.middle_panel, self.class_icons, self.middle_rows, 1)
            self.draw(self.middle_panel, self.damage_type_label, self.middle_rows, 0)
            self.draw(self.middle_panel, self.damage_icons, self.middle_rows, 1)

        # Master Hero #
        elif card_type_index == 4:
            self.draw(self.left_panel, self.cost_label, self.left_rows, 0)
            self.draw(self.left_panel, self.cost_text, self.left_rows, 1)
            self.draw(self.left_panel, self.instant_label, self.left_rows, 0)
            self.draw(self.left_panel, self.instant_checkbox, self.left_rows, 1)
            self.draw(self.left_panel, self.faction_label, self.left_rows, 0)
            self.draw(self.left_panel, self.faction_icons, self.left_rows, 1)
            self.draw(self.left_panel, self.health_label, self.left_rows, 0)
            self.draw(self.left_panel, self.health_text, self.left_rows, 1)
            self.draw(self.left_panel, self.damage_label, self.left_rows, 0)
            self.draw(self.left_panel, self.damage_text, self.left_rows, 1)    

            self.draw(self.middle_panel, self.class_label, self.middle_rows, 0)
            self.draw(self.middle_panel, self.class_icons, self.middle_rows, 1)
            self.draw(self.middle_panel, self.damage_type_label, self.middle_rows, 0)
            self.draw(self.middle_panel, self.damage_icons, self.middle_rows, 1)

        # Location #
        elif card_type_index == 5:
            self.draw(self.left_panel, self.variant_label, self.left_rows, 0)
            self.draw(self.left_panel, self.variant_combo_box, self.left_rows, 1)
            self.draw(self.left_panel, self.faction_label, self.left_rows, 0)
            self.draw(self.left_panel, self.faction_icons, self.left_rows, 1)
            self.draw(self.left_panel, self.capacity_label, self.left_rows, 0)
            self.draw(self.left_panel, self.capacity_text, self.left_rows, 1)

            self.draw(self.middle_panel, self.class_label, self.middle_rows, 0)
            self.draw(self.middle_panel, self.class_icons, self.middle_rows, 1)

        # Quest or Event #
        elif card_type_index in [6, 7]:
            self.draw(self.left_panel, self.variant_label, self.left_rows, 0)
            self.draw(self.left_panel, self.variant_combo_box, self.left_rows, 1)
            self.draw(self.left_panel, self.faction_label, self.left_rows, 0)
            self.draw(self.left_panel, self.faction_icons, self.left_rows, 1)
        
            self.draw(self.middle_panel, self.class_label, self.middle_rows, 0)
            self.draw(self.middle_panel, self.class_icons, self.middle_rows, 1)

        self.draw(self.left_panel, self.effect_label, self.left_rows, 0)
        self.draw(self.left_panel, self.effect_text, self.left_rows, 1)
        self.draw(self.left_panel, self.flavour_label, self.left_rows, 0)
        self.draw(self.left_panel, self.flavour_text, self.left_rows, 1)
        self.draw(self.left_panel, self.background_label, self.left_rows, 0)
        self.draw(self.left_panel, self.background_combo_box, self.left_rows, 1)

        self.draw(self.middle_panel, self.subtypes_label, self.middle_rows, 0)
        self.draw(self.middle_panel, self.subtypes_text, self.middle_rows, 1)
        self.draw(self.middle_panel, self.tags_label, self.middle_rows, 0)
        self.draw(self.middle_panel, self.tags_text, self.middle_rows, 1)
        self.draw(self.middle_panel, self.rarity_label, self.middle_rows, 0)
        self.draw(self.middle_panel, self.rarity_combo_box, self.middle_rows, 1)
        self.draw(self.middle_panel, self.block_number_label, self.middle_rows, 0)
        self.draw(self.middle_panel, self.block_number_text, self.middle_rows, 1)
        self.draw(self.middle_panel, self.set_name_label, self.middle_rows, 0)
        self.draw(self.middle_panel, self.set_name_text, self.middle_rows, 1)
        self.draw(self.middle_panel, self.set_number_label, self.middle_rows, 0)
        self.draw(self.middle_panel, self.set_number_widget, self.middle_rows, 1)

    def redrawPreview(self, manual = False):
        if not manual:
            self.preview_table = int(self.preview_array.index(self.card_preview) / 24)
        self.preview_row = 0
        self.preview_column = 0

        self.clear(self.preview_layout)
        self.clear(self.hidden_preview_layout)

        preview_max_range = min(len(self.preview_array), (self.preview_table + 1) * 24)
        preview_range = range(self.preview_table * 24, preview_max_range)

        for preview_index in range(len(self.preview_array)):
            preview = self.preview_array[preview_index]
            if preview_index in preview_range:
                if self.preview_column == 4:
                    self.preview_row += 1
                    self.preview_column = 0
                self.preview_layout.addWidget(preview, self.preview_row, self.preview_column)
                self.preview_column += 1
            else:
                self.hidden_preview_layout.addWidget(preview)

        while self.preview_row != 5 or self.preview_column != 4:
            if self.preview_column == 4:
                self.preview_row += 1
                self.preview_column = 0
            self.preview_layout.addWidget(self.emptySlotLabel(), self.preview_row, self.preview_column)
            self.preview_column += 1

        self.previewClicked(self.card_preview)

    def emptySlotLabel(self):
        empty_slot_label = QLabel()
        empty_slot_label.setPixmap(self.empty_slot_image)

        return empty_slot_label

    def nameChanged(self):
        self.card.name = self.name_text.text()

        if self.lockGeneration():
            self.generateCard()
            self.unlockGeneration()

    def cardTypeChanged(self):
        type_index = self.type_combo_box.currentIndex()

        for index in reversed(range(self.variant_combo_box.count())):
            self.variant_combo_box.removeItem(index)

        if type_index == 0:
            self.variant_combo_box.addItems(self.locale["variant_names"][0:2])
            self.card.variant = self.variant_names[0]
            self.variant_combo_box.addItems(os.listdir("Template/Custom/Ability"))
        elif type_index == 1:
            self.variant_combo_box.addItems(self.locale["variant_names"][0:2])
            self.card.variant = self.variant_names[0]
            self.variant_combo_box.addItems(os.listdir("Template/Custom/Ally"))
        elif type_index == 2:
            self.variant_combo_box.addItems(self.locale["variant_names"][4:7])
            self.variant_combo_box.addItem(self.locale["variant_names"][1])
            self.card.variant = self.variant_names[4]
            self.variant_combo_box.addItems(os.listdir("Template/Custom/Equipment"))
        elif type_index == 3:
            self.variant_combo_box.addItems(self.locale["variant_names"][2:4])
            self.card.variant = self.variant_names[2]
            self.variant_combo_box.addItems(os.listdir("Template/Custom/Hero"))
        elif type_index == 4:
            self.variant_combo_box.addItems(self.locale["variant_names"][0:1])
            self.card.variant = self.variant_names[0]
            self.variant_combo_box.addItems(os.listdir("Template/Custom/Ability"))
        elif type_index == 5:
            self.variant_combo_box.addItems(self.locale["variant_names"][0:1])
            self.card.variant = self.variant_names[0]
            self.variant_combo_box.addItems(os.listdir("Template/Custom/Location"))
        elif type_index in [6, 7]:
            self.variant_combo_box.addItems(self.locale["variant_names"][0:1])
            self.card.variant = self.variant_names[0]
            self.variant_combo_box.addItems(os.listdir("Template/Custom/Quest"))

        self.card.card_type = self.type_names[type_index]

        self.clearGUI()
        self.drawGUI()

        if self.lockGeneration():
            self.card_art_handler.computeRubberband(False, True)
            self.generateCard()
            self.unlockGeneration()

    def typeOverrideChanged(self):
        self.card.type_override = self.type_override_text.text()

        if self.lockGeneration():
            self.generateCard()
            self.unlockGeneration()

    def variantChanged(self):
        if self.variant_combo_box.currentText():
            if self.variant_combo_box.currentText() in self.locale["variant_names"]:
                variant_index = self.locale["variant_names"].index(self.variant_combo_box.currentText())
                self.card.variant = self.variant_names[variant_index]
            else:
                self.card.variant = self.variant_combo_box.currentText()

        if self.lockGeneration():
            self.card_art_handler.computeRubberband(False, True)
            self.generateCard()
            self.unlockGeneration()

    def factionToggled(self, name):
        button = self.faction_button_dict[name]
        if name != self.current_faction_button.name:
            self.current_faction_button.toggled = False
            self.current_faction_button.setPixmap(self.current_faction_button.inactive)
            self.current_faction_button = button
        if button.toggled:
            self.card.faction = name
        else:
            self.card.faction = ""

        if self.lockGeneration():
            self.generateCard()
            self.unlockGeneration()

    def costChanged(self):
        self.card.cost = self.cost_text.text()

        if self.lockGeneration():
            self.generateCard()
            self.unlockGeneration()

    def instantChanged(self):
        self.card.instant = self.instant_checkbox.isChecked()

        if self.lockGeneration():
            self.generateCard()
            self.unlockGeneration()

    def healthChanged(self):
        self.card.health = self.health_text.text()

        if self.lockGeneration():
            self.generateCard()
            self.unlockGeneration()

    def damageChanged(self):
        self.card.damage = self.damage_text.text()

        if self.lockGeneration():
            self.generateCard()
            self.unlockGeneration()

    def capacityChanged(self):  
        self.card.capacity = self.capacity_text.text()

        if self.lockGeneration():
            self.generateCard()
            self.unlockGeneration()

    def armorChanged(self):
        self.card.armor = self.armor_text.text()

        if self.lockGeneration():
            self.generateCard()
            self.unlockGeneration()

    def strikeCostChanged(self):
        self.card.strike_cost = self.strike_cost_text.text()

        if self.lockGeneration():
            self.generateCard()
            self.unlockGeneration()

    def subtypesChanged(self):
        self.card.subtypes = self.subtypes_text.text()

        if self.lockGeneration():
            self.generateCard()
            self.unlockGeneration()

    def tagsChanged(self):
        self.card.tags = self.tags_text.text()

        if self.lockGeneration():
            self.generateCard()
            self.unlockGeneration()

    def effectChanged(self):
        self.card.effect = self.effect_text.toPlainText()

        if self.lockGeneration():
            self.generateCard()
            self.unlockGeneration()

    def flavourChanged(self):
        self.card.flavour = self.flavour_text.toPlainText()

        if self.lockGeneration():
            self.generateCard()
            self.unlockGeneration()

    def backgroundChanged(self):
        self.card.background_path = "Template/Custom/Background/" + self.background_combo_box.currentText()

        if self.lockGeneration():
            self.generateCard()
            self.unlockGeneration()

    def rarityChanged(self):
        rarity_index = self.rarity_combo_box.currentIndex()
        self.card.rarity = self.rarity_names[rarity_index]

        if self.lockGeneration():
            self.generateCard()
            self.unlockGeneration()

    def blockNumberChanged(self):
        self.card.block_number = self.block_number_text.text()

        if self.lockGeneration():
            self.generateCard()
            self.unlockGeneration()

    def setNameChanged(self):
        self.card.set_name = self.set_name_text.text()

        if self.lockGeneration():
            self.generateCard()
            self.unlockGeneration()

    def setNumberChanged(self):
        if not self.set_number_text.text().isdigit():
            self.set_number_text.setText("")
            return

        self.card.set_number = self.set_number_text.text()

        preview_set = False

        self.preview_array.remove(self.card_preview)
        if not self.card.set_number:
            self.card.set_number = "0"
            self.preview_array.insert(0, self.card_preview)
            preview_set = True
        else:
            for index, preview in enumerate(self.preview_array):
                if int(preview.card.set_number) >= int(self.card.set_number):
                    self.preview_array.insert(index, self.card_preview)
                    preview_set = True
                    break

        if not preview_set:
            self.preview_array.append(self.card_preview)

        if self.lockGeneration():
            self.generateCard()
            self.unlockGeneration()

        self.redrawPreview()

    def setMaxNumberChanged(self):
        if not self.set_max_number_text.text().isdigit():
            self.set_max_number_text.setText("")
            return

        self.card.set_max_number = self.set_max_number_text.text()

        if self.lockGeneration():
            self.generateCard()
            self.unlockGeneration()

    def artistChanged(self):
        self.card.artist = self.artist_text.text()

        if self.lockGeneration():
            self.generateCard()
            self.unlockGeneration()

    def copyrightChanged(self):
        self.card.copyright = self.copyright_text.text()

        if self.lockGeneration():
            self.generateCard()
            self.unlockGeneration()

    def borderSharpChanged(self):
        self.card.border_sharp = self.border_sharp_checkbox.isChecked()

        if self.lockGeneration():
            self.generateCard()
            self.unlockGeneration()

    def bleedingXChanged(self):
        if self.bleeding_x_text.text():
            if not self.bleeding_x_text.text().isdigit():
                self.bleeding_x_text.setText("")
            else:
                if self.bleeding[0] == 1500:
                    return

                self.bleeding[0] = min(1500, int(self.bleeding_x_text.text()))
        else:
            if self.bleeding[0] == 750:
                return

            self.bleeding[0] = 750

        if self.lockGeneration():
            for preview in self.preview_array:
                self.generateCard(preview, self.card_preview == preview)
            self.unlockGeneration()

    def bleedingYChanged(self):
        if self.bleeding_y_text.text():
            if not self.bleeding_y_text.text().isdigit():
                self.bleeding_y_text.setText("")
            else:
                if self.bleeding[1] == 2100:
                    return

                self.bleeding[1] = min(2100, int(self.bleeding_y_text.text()))
        else:
            if self.bleeding[1] == 1050:
                return

            self.bleeding[1] = 1050

        if self.lockGeneration():
            for preview in self.preview_array:
                self.generateCard(preview, self.card_preview == preview)
            self.unlockGeneration()

    def classToggled(self, name):
        if name not in self.card.class_icons:
            self.card.class_icons.append(name)
        else:
            self.card.class_icons.remove(name)

        if self.lockGeneration():
            self.generateCard()
            self.unlockGeneration()

    def damageToggled(self, name):
        button = self.damage_button_dict[name]
        if self.current_damage_button and name != self.current_damage_button.name:
            self.current_damage_button.toggled = False
            self.current_damage_button.setPixmap(self.current_damage_button.inactive)
            self.current_damage_button = button
        if button.toggled:
            self.card.damage_type = name
            self.current_damage_button = button
        else:
            self.card.damage_type = ""
            self.current_damage_button = None

        if self.lockGeneration():
            self.generateCard()
            self.unlockGeneration()

    def selectArt(self):
        filename, _ = QFileDialog.getOpenFileName(dir=self.config["default_directory"], caption="Select Art File", filter=("Images (*.png *.jpg *.jpeg);;All files (*)"))
        if filename:
            self.card.art_path = filename
            pil_image = Image.open(filename)
            image = QPixmap.fromImage(ImageQt(pil_image).scaledToHeight(500, QtCore.Qt.SmoothTransformation))
            self.card_art_handler.setArt(image, pil_image.height)
            pil_image.close()
        else:
            print("Invalid art file!")

    def resizeArt(self):
        if self.resize_art_frame.isVisible():
            self.resize_art_frame.hide()
        else:
            self.resize_art_frame.show()

    def generateCard(self, card_preview=None, switch_select=True):
        if not card_preview:
            card_preview = self.card_preview
        
        generated_image = self.wcg.createCard(card_preview.card, self.bleeding).copy()

        if card_preview == self.card_preview:
            full_image = ImageQt(generated_image).scaledToHeight(500, QtCore.Qt.SmoothTransformation)
            full_image = QPixmap.fromImage(full_image)
            self.card_image.setPixmap(full_image)

        preview_image = QPixmap.fromImage(ImageQt(generated_image))
        preview_image = preview_image.scaledToWidth(100, QtCore.Qt.SmoothTransformation)
        card_preview.preview_image = preview_image

        if switch_select:
            generated_image.alpha_composite(self.selected_border.resize((max(750, self.bleeding[0]), max(1050, self.bleeding[1])), 1), (0, 0))
        generated_image = QPixmap.fromImage(ImageQt(generated_image))
        generated_image = generated_image.scaledToWidth(100, QtCore.Qt.SmoothTransformation)
        card_preview.selected_preview_image = generated_image
        
        card_preview.setPixmap(generated_image)

    def saveCard(self):
        filename, _ = QFileDialog.getSaveFileName(dir=self.config["default_directory"], caption="Select Card Save Location", filter=("Image (*.png)"))
        if filename:
            save_path = filename
            if filename[-4:] != ".png":
                save_path += ".png"
            self.card.image.save(save_path)
        else:
            print("Invalid save path!")

    def saveAllCards(self):
        save_path = QFileDialog.getExistingDirectory(dir=self.config["default_directory"], caption="Select a save directory")
        if save_path:
            default_name = 0
            for preview in self.preview_array:
                card = preview.card
                if card.name:
                    card_name = card.name
                    if card.card_type == "Hero" and card.variant == "Back":
                        card_name = card_name + " - Backside"
                else:
                    card_name = str(default_name)
                    default_name += 1
                
                self.wcg.createCard(card, self.bleeding).save(save_path + "/" + card_name + ".png")
        else:
            print("Invalid save path!")

    def saveToPrintable(self):
        save_path = QFileDialog.getExistingDirectory(dir=self.config["default_directory"], caption="Select a save directory")
        if save_path:
            default_name = 0
            printable_image = Image.new("RGBA", (self.bleeding[0] * 3, self.bleeding[1] * 3))
            pending = False
            for index, preview in enumerate(self.preview_array):
                pending = True
                card = preview.card
                if card.name:
                    card_name = card.name
                    if card.card_type == "Hero" and card.variant == "Back":
                        card_name = card_name + " - Backside"
                else:
                    card_name = str(default_name)
                    default_name += 1

                self.wcg.createCard(card, self.bleeding)
                printable_image.alpha_composite(card.image, (self.bleeding[0]  * (index % 3), self.bleeding[1] * int((index % 9) / 3)))

                if index % 9 == 8:
                    printable_image.save(save_path + "/" + "printable_" + str(int(index / 9)) + ".png")
                    printable_image = Image.new("RGBA", (self.bleeding[0] * 3, self.bleeding[1] * 3))
                    pending = False

            if pending:
                printable_image.save(save_path + "/" + "printable_" + str(int(len(self.preview_array) / 9)) + ".png")
        else:
            print("Invalid save path!")

    def loadFromJSON(self):
        json_path, _ = QFileDialog.getOpenFileName(dir=self.config["default_directory"], caption="Select JSON File", filter=("JSON (*.json);;All files (*)"))
        if os.path.isfile(json_path):
            art_base_path = json_path[:-5] + " Arts/"

            try:
                with open(json_path, "r", encoding="utf-8") as card_file:
                    card_dict = json.load(card_file)
                    for card_key in card_dict:
                        card_json = card_dict[card_key]
                        card = Card()
                        card.fromJson(card_json)
                        if card.art_path != "Template/Dummy.png":
                            card.art_path = art_base_path + card.art_path
                        self.createCard(card)
                
                self.redrawPreview()
                QMessageBox.information(self.main_frame, "Load Json", "Json file loaded.")

            except Exception as e:
                QMessageBox.information(self.main_frame, "Load Json", "Could not load Json file.\nError is : " + str(e))
                print("Could not load Json file at:", json_path)
        else:
            print("Invalid Json file path!")

    def saveToJSON(self):
        save_path, _ = QFileDialog.getSaveFileName(dir=self.config["default_directory"], caption="Select JSON Save Location", filter=("JSON (*.json);;All files (*)"))
        local_art_folder_path = os.path.basename(save_path)

        if save_path[-5:] != ".json":
            save_path = save_path + ".json"
        else:
            local_art_folder_path = local_art_folder_path[:-5]
        local_art_folder_path += " Arts/"

        save_art_folder = save_path[:-5] + " Arts"
        if not os.path.isdir(save_art_folder):
            os.mkdir(save_art_folder)

        last_card_name = ""

        try:
            with open(save_path + ".tmp", "w", encoding="utf-8") as json_file:
                json_card_list = {}
                card_id = 0
                for preview in self.preview_array:
                    card = preview.card
                    last_card_name = card.name
                    if card.art_path != "Template/Dummy.png":
                        art_base_name = os.path.basename(card.art_path)
                        art_save_path = save_art_folder + "/" + art_base_name
                        if card.art_path != art_save_path:
                            shutil.copy(card.art_path, art_save_path)
                    json_card_list[card_id] = card.toJson()
                    if card.art_path != "Template/Dummy.png":
                        json_card_list[card_id]["art_path"] = art_base_name
                    card_id += 1
                json.dump(json_card_list, json_file, indent="\t")

            shutil.copy(save_path + ".tmp", save_path)
            os.remove(save_path + ".tmp") 
            QMessageBox.information(self.main_frame, "Save Json", "Json file saved.")

        except Exception:
            QMessageBox.information(self.main_frame, "Save Json", "Could not save Json file.")
            print("Could not save Json because of", last_card_name)

    def languageChanged(self):
        locale_file = open("Locales/" + self.language_combo_box.currentText() + ".json", "r", encoding="utf-8")
        self.locale = json.load(locale_file)
        locale_file.close()

        self.lockGeneration()

        self.name_label.setText(self.locale["name"])
        self.type_label.setText(self.locale["type"])
        self.type_override_label.setText(self.locale["type_override"])
        self.variant_label.setText(self.locale["variant"])
        self.cost_label.setText(self.locale["cost"])
        self.instant_label.setText(self.locale["instant"])
        self.faction_label.setText(self.locale["faction"])
        self.health_label.setText(self.locale["health"])
        self.damage_label.setText(self.locale["damage"])
        self.strike_cost_label.setText(self.locale["strike_cost"])
        self.armor_label.setText(self.locale["armor"])
        self.capacity_label.setText(self.locale["capacity"])
        self.effect_label.setText(self.locale["effect"])
        self.flavour_label.setText(self.locale["flavour"])
        self.class_label.setText(self.locale["classes"])
        self.damage_type_label.setText(self.locale["damage_type"])
        self.subtypes_label.setText(self.locale["subtypes"])
        self.tags_label.setText(self.locale["tags"])
        self.rarity_label.setText(self.locale["rarity"])
        self.block_number_label.setText(self.locale["block_number"])
        self.set_name_label.setText(self.locale["set_name"])
        self.set_number_label.setText(self.locale["set_number"])
        self.import_art_button.setText(self.locale["import_art"])
        self.resize_art_button.setText(self.locale["resize_art"])
        self.save_card_button.setText(self.locale["save_card"])
        self.save_to_printable_button.setText(self.locale["save_to_printable"])
        self.artist_label.setText(self.locale["artist"])
        self.copyright_label.setText(self.locale["copyright"])
        self.border_sharp_label.setText(self.locale["sharp"])
        self.border_label.setText(self.locale["border"])
        self.bleeding_label.setText(self.locale["bleeding"])
        self.json_save_button.setText(self.locale["save_json"])
        self.json_load_button.setText(self.locale["load_json"])
        self.language_label.setText(self.locale["language"])
        self.font_label.setText(self.locale["font"])
        self.help_button.setText(self.locale["help"])
        self.help_text.setText(self.locale["help_text"])

        # Store combobox indexes #
        old_indexes = []

        # Change Rarity language #
        old_indexes.append(self.rarity_combo_box.currentIndex())
        for index in reversed(range(self.rarity_combo_box.count())):
            self.rarity_combo_box.removeItem(index)
        self.rarity_combo_box.addItems(self.locale["rarity_names"])

        # Change Type language #
        old_indexes.append(self.type_combo_box.currentIndex())
        old_indexes.append(self.variant_combo_box.currentIndex())
        for index in reversed(range(self.type_combo_box.count())):
            self.type_combo_box.removeItem(index)
        self.type_combo_box.addItems(self.locale["type_names"])

        self.unlockGeneration()

        # Check if language changed by user input or preview click #
        if not self.nonManualLanguageSwitch:
            # Restore combobox indexes #
            self.rarity_combo_box.setCurrentIndex(old_indexes[0])
            self.type_combo_box.setCurrentIndex(old_indexes[1])
            self.variant_combo_box.setCurrentIndex(old_indexes[2])

        self.card.locale_name = self.language_combo_box.currentText()

        self.generateCard()

    def borderClicked(self):
        if self.border_frame.isVisible():
            self.border_frame.hide()
        else:
            self.border_frame.show()

    def borderColorChanged(self, color):
        self.card.border_color = (color.red(), color.green(), color.blue(), 255)
        self.border_color_button.setStyleSheet("QPushButton{background-color : #" + "%0.2x" % color.red() + "%0.2x" % color.green() + "%0.2x" % color.blue() + ";}")

        if self.lockGeneration():
            self.generateCard()
            self.unlockGeneration()

    def helpClicked(self):
        if self.help_frame.isVisible():
            self.help_frame.hide()
        else:
            self.help_frame.show()

    def createCard(self, card = None):
        if card:
            new_card = card
        else:
            new_card = Card()
            new_card.locale_name = self.card.locale_name

        new_card_preview = CardPreview(new_card)
        new_card_preview.toggle_signal.connect(self.previewClicked)
        
        if not card:
            # Keep card type #
            new_card.card_type = self.card.card_type
            # Keep faction #
            new_card.faction = self.card.faction
            # Keep variant #
            new_card.variant = self.card.variant
            # Keep and update set related informations #
            new_card.block_number = self.card.block_number
            new_card.set_name = self.card.set_name
            new_card.set_number = str(len(self.preview_array) + 1)
            set_size = new_card.set_number
            str_set_size = str(set_size)
            new_card.set_max_number = str_set_size
            for preview in self.preview_array:
                preview.card.set_max_number = str_set_size

            # Keep border style #
            new_card.border_color = self.card.border_color

        else:
            self.nonManualLanguageSwitch = True

        self.generateCard(new_card_preview)

        preview_set = False
        
        if card:
            new_card_preview.setPixmap(new_card_preview.preview_image)
            if not self.card.set_number:
                self.card.set_number = "0"
                self.preview_array.insert(0, new_card_preview)
                preview_set = True
            else:
                for index in range(len(self.preview_array)):
                    if int(self.preview_array[index].card.set_number) >= int(new_card_preview.card.set_number):
                        self.preview_array.insert(index, new_card_preview)
                        preview_set = True
                        break

        if not preview_set:
            self.preview_array.append(new_card_preview)

        self.nonManualLanguageSwitch = False

    def createCardClicked(self):
        self.createCard()
        self.redrawPreview()
        self.previewClicked(self.preview_array[-1])

    def deleteCardClicked(self):
        preview_size = len(self.preview_array)
        # Set preview to previous card #
        preview_index = self.preview_array.index(self.card_preview)
        if preview_index > 0:
            preview_index -= 1
        self.preview_array.remove(self.card_preview)

        if preview_size == 1:
            self.createCard(Card())
        
        self.previewClicked(self.preview_array[preview_index])
        self.redrawPreview()

    def previousPreviewTableClicked(self):
        if self.preview_table > 0:
            self.preview_table -= 1
            self.redrawPreview(True)

    def nextPreviewTableClicked(self):
        if self.preview_table < int((len(self.preview_array) - 1) / 24):
            self.preview_table += 1
            self.redrawPreview(True)

    def previewClicked(self, card_preview):
        changed = card_preview != self.card_preview
        if changed:
            self.card_preview.setPixmap(self.card_preview.preview_image)
            self.card_preview = card_preview
            self.card = self.card_preview.card

            pil_image = Image.open(self.card.art_path)
            image = QPixmap.fromImage(ImageQt(pil_image).scaledToHeight(500, QtCore.Qt.SmoothTransformation))
            self.card_art_handler.setArt(image, pil_image.height, False)
            ratio = pil_image.height / 500
            pil_image.close()

            if self.card.art_box:
                self.card_art_handler.ax = self.card.art_box[0] / ratio
                self.card_art_handler.ay = self.card.art_box[1] / ratio
                self.card_art_handler.bx = self.card.art_box[2] / ratio
                self.card_art_handler.by = self.card.art_box[3] / ratio

                # Safeguard for ratio computation when loading #
                if self.card_art_handler.ax < 0:
                    self.card_art_handler.ax = 0
                if self.card_art_handler.ay < 0:
                    self.card_art_handler.ax = 0
                if self.card_art_handler.bx >= self.card_art_handler.max_x:
                    self.card_art_handler.bx = self.card_art_handler.max_x - 1
                if self.card_art_handler.by >= self.card_art_handler.max_y:
                    self.card_art_handler.by = self.card_art_handler.max_y - 1

            self.card_art_handler.computeRubberband(True, True)

            self.generateCard()

        if len(self.preview_array) == 1 or changed:
            self.nonManualLanguageSwitch = True

            self.loadCardState()

            self.nonManualLanguageSwitch = False

    def defaultDirectoryClicked(self):
        selected_directory = QFileDialog.getExistingDirectory()
        if(selected_directory):
            self.config["default_directory"] = selected_directory
            config_file = open("config.json", "w+", encoding="utf-8")
            json.dump(self.config, config_file, indent="\t")
            config_file.close()

    def fontMinusButtonClicked(self):
        font = self.app.font()
        self.font_size = max(1, self.font_size - 1)
        font.setPointSize(self.font_size)
        self.app.setFont(font)

        self.config["font_size"] = self.font_size
        config_file = open("config.json", "w+", encoding="utf-8")
        json.dump(self.config, config_file, indent="\t")
        config_file.close()

    def fontPlusButtonClicked(self):
        font = self.app.font()
        self.font_size = min(40, self.font_size + 1)
        font.setPointSize(self.font_size)
        self.app.setFont(font)

        self.config["font_size"] = self.font_size
        config_file = open("config.json", "w+", encoding="utf-8")
        json.dump(self.config, config_file, indent="\t")
        config_file.close()


    def loadCardState(self):
        self.lockGeneration()
        # Name #
        self.name_text.setText(self.card.name)
        # Language #
        res = self.language_combo_box.findText(self.card.locale_name)
        if res < 0:
            res = 0
        self.language_combo_box.setCurrentIndex(res)
        # Store variant #
        variant = self.card.variant
        # Type #
        if self.card.card_type in self.type_names:
            res = self.type_combo_box.findText(self.locale["type_names"][self.type_names.index(self.card.card_type)])
        else:
            res = 0
        self.type_combo_box.setCurrentIndex(res)
        # Type override #
        self.type_override_text.setText(self.card.type_override)
        # Faction #
        if self.card.faction in self.faction_button_dict.keys():
            self.faction_button_dict[self.card.faction].mousePressEvent(None)
        else:
            self.faction_button_dict["Neutral"].mousePressEvent(None)
        # Trigger type changed #
        self.cardTypeChanged()
        # Reapply variant #
        self.card.variant = variant
        if self.card.variant == "":
            self.variant_combo_box.setCurrentIndex(0)
        else:
            if self.card.variant in self.locale["variant_names"]:
                self.variant_combo_box.setCurrentIndex(self.variant_combo_box.findText(self.locale["variant_names"][self.variant_names.index(self.card.variant)]))
            else:
                res = self.variant_combo_box.findText(self.card.variant)
                if res < 0:
                    res = 0
                self.variant_combo_box.setCurrentIndex(res)
        # Classes #
        for key in self.class_button_dict:
            if key in self.card.class_icons:
                self.class_button_dict[key].activate()
            else:
                self.class_button_dict[key].deactivate()
        self.cost_text.setText(self.card.cost)
        self.damage_text.setText(self.card.damage)
        if self.card.damage_type:
            self.damage_button_dict[self.card.damage_type].activate()
            self.damageToggled(self.damage_button_dict[self.card.damage_type].name)
        elif self.current_damage_button:
            self.current_damage_button.mousePressEvent(None)
        self.health_text.setText(self.card.health)
        self.armor_text.setText(self.card.armor)
        self.strike_cost_text.setText(self.card.strike_cost)
        self.capacity_text.setText(self.card.capacity)
        self.instant_checkbox.setChecked(self.card.instant)
        self.subtypes_text.setText(self.card.subtypes)
        self.tags_text.setText(self.card.tags)
        # Use plain text to avoid interpretation of bold, italic, etc #
        self.effect_text.setPlainText(self.card.effect)
        self.flavour_text.setPlainText(self.card.flavour)
        self.artist_text.setText(self.card.artist)
        self.copyright_text.setText(self.card.copyright)
        self.rarity_combo_box.setCurrentIndex(self.rarity_combo_box.findText(self.locale["rarity_names"][self.rarity_names.index(self.card.rarity)]))
        self.block_number_text.setText(self.card.block_number)
        self.set_name_text.setText(self.card.set_name)
        self.set_number_text.setText(self.card.set_number)
        self.set_max_number_text.setText(self.card.set_max_number)
        self.border_color_dialog.setCurrentColor(QColor(self.card.border_color[0], self.card.border_color[1], self.card.border_color[2]))
        self.border_color_button.setStyleSheet("QPushButton{background-color : #" + "%0.2x" % self.card.border_color[0] + "%0.2x" % self.card.border_color[1] + "%0.2x" % self.card.border_color[2] + ";}")
        self.border_sharp_checkbox.setChecked(self.card.border_sharp)

        self.unlockGeneration()

    def lockGeneration(self):
        if self.lock_generation:
            return False
        else:
            self.lock_generation = True
            return True

    def unlockGeneration(self):
        self.lock_generation = False

WoWCardGeneratorGUI().run()
